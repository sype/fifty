#!/bin/bash -e


PID=$$
SCRIPTNAME=$(basename $0)
MAX_EXECUTION_TIME_HOURS=4
TMPFILE=shellscript.${SCRIPTNAME%.*}.$MAX_EXECUTION_TIME_HOURS.$PID
touch /home/runner/logs/$TMPFILE
export PATH=/usr/bin:/bin:/usr/local/mysql/bin/

shopt -s expand_aliases
source /home/runner/.bashrc
cd /home/runner/muliBase
mkdir -p ./backups/exports
YESTERDAY=`date --date="1 days ago" +%Y%m%d`
DATE=`date +"%a"`
DAY=`date +"%w"`
# $DATE vaut par exemple lun.
# $DAY = 0..6, 0 est dimanche

# BACKUP ON FTP SERVER
echo "### START :  `date +"%H:%M"` ###"

# GET DATABASES TO BACKUP
DATABASES=`mysql -udb_user -p2store4GOOD --batch -e 'SHOW DATABASES LIKE "DB_%"' | tail -n +2`
echo "databases to backup : $DATABASES"
# 1. KILL TASKLAUNCHERS
# PIDS=`ps aux | grep launchTasks\.sh | grep -v grep | awk '{ print $2 }'`
PIDS=`pgrep launchTasks\.sh || true;`
for PID in $PIDS
do
        # delete the Nagios file
        rm -f /home/runner/logs/shellscript.*$PID
        # kill the pid
        `pkill -TERM -P $PID`
done

# 2. OPTIMIZEDB
# 2.1 DEFRAGMENT TABLES
echo "Defragmenting tables..."
# the last SED is for naming as `DB`.`TABLE` since we can have "-" in tables names
if [[ ! $HOSTNAME = staging* ]]; then
  FRAGMENTED_TABLES="$( mysql -udb_user -p2store4GOOD -e 'SELECT TABLE_SCHEMA,TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA LIKE "DB\_%" AND ENGINE = "InnoDB" AND DATA_FREE > 0 AND TABLE_NAME NOT IN ("FETCHED_DATA")' | grep -v "^+" | grep -v "TABLE_SCHEMA.TABLE_NAME" | sed "s,\t,.," | sed "s,\(.*\)\.\(.*\),\`\1\`.\`\2\`," )"
  echo "${FRAGMENTED_TABLES}" | parallel 'mysql -udb_user -p2store4GOOD -e "ALTER TABLE {} ENGINE=INNODB;"'
fi

# 2.2 CLEANING TABLES
for DATABASE in $DATABASES; do
  echo Cleaning $DATABASE...
  # DROP ALL TMP_JOIN TABLES
  TMPTABLES="$( mysql --disable-column-names --silent -udb_user -p2store4GOOD -e 'SHOW TABLES FROM '$DATABASE' LIKE "TMP\_JOIN%"' )"
  if [ -z "$TMPTABLES" ]
  then
    echo "NO TMP_JOIN TABLES"
  else
    echo "$TMPTABLES" | parallel 'mysql -udb_user -p2store4GOOD '$DATABASE' -e "DROP TABLE IF EXISTS \`{}\`;"'
  fi

  mysql --force -udb_user -p2store4GOOD $DATABASE < cleanDB.sql

  # EMPTY ALL TMP TABLES
  TMPTABLES="$( mysql --disable-column-names --silent -udb_user -p2store4GOOD -e 'select TABLE_NAME from information_schema.tables where table_schema="'$DATABASE'" and (table_name like "TMP\_%" or table_name like "%\_TMP");' )"
  if [ -z "$TMPTABLES" ]
  then
    echo "NO TMP_* TABLES"
  else
    echo "$TMPTABLES" | parallel 'mysql -udb_user -p2store4GOOD '$DATABASE' -e "DELETE FROM \`{}\`;"'
  fi
done

# 3. DUMPS DB, APACHE CONFIG & WEBAPPS
for DATABASE in $DATABASES; do
 echo "Dumping $DATABASE"
 mysqldump --max_allowed_packet=512M --quick --extended-insert --compress --single-transaction --force -udb_user -p2store4GOOD $DATABASE  | gzip > ./backups/dump.$DATABASE.${DAY}.txt.gz
 
# stop building FAILOVER_ database

done

zip -q ./backups/dump.config_web.${DAY}.zip /etc/apache2/sites-enabled/*
if ls -1 /var/lib/tomcat6/webapps/*.war >/dev/null 2>&1
then
 zip -q ./backups/dump.webapps.${DAY}.zip /var/lib/tomcat6/webapps/*.war /var/www*
else 
 zip -q ./backups/dump.webapps.${DAY}.zip /home/tomcat6/webapps/*.war /var/www*
fi

# backup TRAC is exists
if [ -d "/home/runner/trac" ]; then
  zip -rq ./backups/dump.trac.${DAY}.zip /home/runner/trac
fi

# backup the crontab 
crontab -l > ./backups/crontab.${DAY}.txt

# 4. COPY DB TO FTP
cd ./backups

# test if we are saturday or not
if [[ $DAY == "6" ]]
then
  echo "Deleting saturday backup"
  ftpbackup <<END_SCRIPT
  binary
  mdelete *.$DAY.*
  quit
END_SCRIPT
else
  echo "Deleting all but saturday backup"
  ftpbackup <<END_SCRIPT1
  binary
  mdelete *.0.*
  mdelete *.1.*
  mdelete *.2.*
  mdelete *.3.*
  mdelete *.4.*
  mdelete *.5.*
  quit
END_SCRIPT1
fi

# go backup
ftpbackup <<END_SCRIPT2
binary
mput *.$DAY.*
quit
END_SCRIPT2

rm -rf *.$DAY.*
echo "Dumps to FTP : OK"

# 5. EXPORTING TSOS FROM DB AND EXPORTS, FOR TABLEAU
for DATABASE in $DATABASES; do
  TSO_TABLES=`mysql --disable-column-names -udb_user -p2store4GOOD -e 'SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = "'$DATABASE'" AND TABLE_ROWS != 0 AND TABLE_NAME LIKE "TSO\_\_%"'`
  DSK_TABLES=`mysql --disable-column-names -udb_user -p2store4GOOD -e 'SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = "'$DATABASE'" AND TABLE_ROWS != 0 AND TABLE_NAME LIKE "DSK%"'`

  if [ ! -z "$TSO_TABLES" ]
  then
    echo -n Dumping TSOs for $DATABASE
    mysqldump --max_allowed_packet=512M --quick --extended-insert --compress --single-transaction -udb_user -p2store4GOOD $DATABASE $TSO_TABLES "--where=DATE='$YESTERDAY'" | sed '1i\CREATE DATABASE IF NOT EXISTS '$DATABASE' DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;\USE '$DATABASE';' | gzip > /home/runner/muliBase/backups/dump.TSO.$DATABASE.sql.gz
    echo " - END"
  fi

  if [ ! -z "$DSK_TABLES" ]
  then
    echo -n Dumping DSKs for $DATABASE
    mysqldump --max_allowed_packet=512M --quick --extended-insert --compress --single-transaction -udb_user -p2store4GOOD $DATABASE $DSK_TABLES | sed '1i\CREATE DATABASE IF NOT EXISTS '$DATABASE' DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;\USE '$DATABASE';' | gzip > /home/runner/muliBase/backups/dump.TSO.DSK.$DATABASE.sql.gz
    echo " - END"
  fi

  # ON REMET LES TASKS QUI ETAIENT EN TRAIN DE S'EXECUTER (PENDING) EN READY
  if [[ ! `mysql --disable-column-names --silent -udb_user -p2store4GOOD $DATABASE -e 'SHOW TABLES LIKE "TASKS"' | wc -l` -eq 0 ]]
  then
	mysql --force -udb_user -p2store4GOOD $DATABASE -e 'UPDATE TASKS SET STATUS="Ready" WHERE STATUS = "Pending" AND TASK_TYPE = "PUBLISH"' 2>&1
  fi
 echo "END tasks ready" 
done

# delete logs directories older than 5 days and tmp files older than 2 days
find /home/publication-log/instances/*/logs/2* -maxdepth 1 -type d -mtime +5 | xargs rm -r 2>&1 || true
find /home/publication-log/instances/*/logs/ -name 'BookByQuery*' -type f -mtime +5 | xargs rm  2>&1 || true
find /home/publication-log/instances/*/tmp -name '*' -type f -mtime +2  -print0 | xargs -0 rm -rf 2>&1 || true
echo "END delete older logs and tmp files"

for INSTANCE in `find /home/publication-log/instances/. -maxdepth 2 -mindepth 2 -name "exports" -type d -printf %P\\\n`; do
  echo $INSTANCE tmp files treatment
  #  echo "Zipping - only directories - exports for $INSTANCE"
  #  zip -q -r /home/runner/muliBase/backups/dump.EXPORTS.$INSTANCE.zip /home/publication-log/instances/$INSTANCE/exports/*

  # copy all EXPORTS in directory and zip them
  find /home/publication-log/instances/$INSTANCE/ -name "*.csv" -type f -exec cp "{}" --target-directory=/home/runner/muliBase/backups/exports \; 2>/dev/null

  # ZIP these exports
  if ls -1 /home/runner/muliBase/backups/exports/*.csv >/dev/null 2>&1
  then
    gzip -q -f /home/runner/muliBase/backups/exports/*.csv 2>/dev/null
  fi

done

# relaunch Tasks for each database if the binary directory exists
for DATABASE in $DATABASES; do
  if [ -d "/home/publication-log/instances/${DATABASE:3}/bin/com" ];
  then
    nohup /home/runner/muliBase/launchTasks.sh ${DATABASE:3} P_DAILYASAP  2>/dev/null  &
    nohup /home/runner/muliBase/launchTasks.sh ${DATABASE:3} P_DAILYONGOING 2>/dev/null  &
    nohup /home/runner/muliBase/launchTasks.sh ${DATABASE:3} P_WEEKLYUPDATE 2>/dev/null  &
    nohup /home/runner/muliBase/launchTasks.sh ${DATABASE:3} P_MONTHLYUPDATE 2>/dev/null  &
    nohup /home/runner/muliBase/launchTasks.sh ${DATABASE:3} P_DELIVERASAP 2>/dev/null  &
    nohup /home/runner/muliBase/launchTasks.sh ${DATABASE:3} P_MEDIA_COST 2>/dev/null  &
    nohup /home/runner/muliBase/launchTasks.sh ${DATABASE:3} P_SURVEY_PUBLICATION 2>/dev/null  &
    nohup /home/runner/muliBase/launchTasks.sh ${DATABASE:3} P_APPLYCOST 2>/dev/null  &
  fi
done
echo END relauch tasks for all instances

# echo date to be checked with Nagios
echo `date +%Y%m%d` > /tmp/backup

rm -f /home/runner/logs/$TMPFILE

echo "### END :  `date +"%H:%M"` ###"
