-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: icinga_web
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cronk`
--

DROP TABLE IF EXISTS `cronk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cronk` (
  `cronk_id` int(11) NOT NULL AUTO_INCREMENT,
  `cronk_uid` varchar(45) DEFAULT NULL,
  `cronk_name` varchar(45) DEFAULT NULL,
  `cronk_description` varchar(100) DEFAULT NULL,
  `cronk_xml` longtext,
  `cronk_user_id` int(11) DEFAULT NULL,
  `cronk_system` tinyint(1) DEFAULT '0',
  `cronk_created` datetime NOT NULL,
  `cronk_modified` datetime NOT NULL,
  PRIMARY KEY (`cronk_id`),
  UNIQUE KEY `cronk_uid_UNIQUE_idx` (`cronk_uid`),
  KEY `cronk_user_id_idx` (`cronk_user_id`),
  CONSTRAINT `cronk_cronk_user_id_nsm_user_user_id` FOREIGN KEY (`cronk_user_id`) REFERENCES `nsm_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cronk`
--

LOCK TABLES `cronk` WRITE;
/*!40000 ALTER TABLE `cronk` DISABLE KEYS */;
/*!40000 ALTER TABLE `cronk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cronk_category`
--

DROP TABLE IF EXISTS `cronk_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cronk_category` (
  `cc_id` int(11) NOT NULL AUTO_INCREMENT,
  `cc_uid` varchar(45) NOT NULL,
  `cc_name` varchar(45) DEFAULT NULL,
  `cc_visible` tinyint(4) DEFAULT '0',
  `cc_position` int(11) DEFAULT '0',
  `cc_system` tinyint(1) DEFAULT '0',
  `cc_created` datetime NOT NULL,
  `cc_modified` datetime NOT NULL,
  PRIMARY KEY (`cc_id`),
  UNIQUE KEY `cc_uid_UNIQUE_idx` (`cc_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cronk_category`
--

LOCK TABLES `cronk_category` WRITE;
/*!40000 ALTER TABLE `cronk_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `cronk_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cronk_category_cronk`
--

DROP TABLE IF EXISTS `cronk_category_cronk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cronk_category_cronk` (
  `ccc_cc_id` int(11) NOT NULL DEFAULT '0',
  `ccc_cronk_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ccc_cc_id`,`ccc_cronk_id`),
  KEY `cronk_category_cronk_ccc_cronk_id_cronk_cronk_id` (`ccc_cronk_id`),
  CONSTRAINT `cronk_category_cronk_ccc_cc_id_cronk_category_cc_id` FOREIGN KEY (`ccc_cc_id`) REFERENCES `cronk_category` (`cc_id`),
  CONSTRAINT `cronk_category_cronk_ccc_cronk_id_cronk_cronk_id` FOREIGN KEY (`ccc_cronk_id`) REFERENCES `cronk` (`cronk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cronk_category_cronk`
--

LOCK TABLES `cronk_category_cronk` WRITE;
/*!40000 ALTER TABLE `cronk_category_cronk` DISABLE KEYS */;
/*!40000 ALTER TABLE `cronk_category_cronk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cronk_principal_category`
--

DROP TABLE IF EXISTS `cronk_principal_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cronk_principal_category` (
  `principal_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`principal_id`,`category_id`),
  KEY `cronk_principal_category_category_id_cronk_category_cc_id` (`category_id`),
  CONSTRAINT `cronk_principal_category_category_id_cronk_category_cc_id` FOREIGN KEY (`category_id`) REFERENCES `cronk_category` (`cc_id`),
  CONSTRAINT `cronk_principal_category_principal_id_nsm_principal_principal_id` FOREIGN KEY (`principal_id`) REFERENCES `nsm_principal` (`principal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cronk_principal_category`
--

LOCK TABLES `cronk_principal_category` WRITE;
/*!40000 ALTER TABLE `cronk_principal_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `cronk_principal_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cronk_principal_cronk`
--

DROP TABLE IF EXISTS `cronk_principal_cronk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cronk_principal_cronk` (
  `cpc_principal_id` int(11) NOT NULL DEFAULT '0',
  `cpc_cronk_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cpc_principal_id`,`cpc_cronk_id`),
  KEY `cronk_principal_cronk_cpc_cronk_id_cronk_cronk_id` (`cpc_cronk_id`),
  CONSTRAINT `ccnp` FOREIGN KEY (`cpc_principal_id`) REFERENCES `nsm_principal` (`principal_id`),
  CONSTRAINT `cronk_principal_cronk_cpc_cronk_id_cronk_cronk_id` FOREIGN KEY (`cpc_cronk_id`) REFERENCES `cronk` (`cronk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cronk_principal_cronk`
--

LOCK TABLES `cronk_principal_cronk` WRITE;
/*!40000 ALTER TABLE `cronk_principal_cronk` DISABLE KEYS */;
/*!40000 ALTER TABLE `cronk_principal_cronk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nsm_db_version`
--

DROP TABLE IF EXISTS `nsm_db_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nsm_db_version` (
  `id` int(11) NOT NULL DEFAULT '0',
  `version` varchar(32) NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nsm_db_version`
--

LOCK TABLES `nsm_db_version` WRITE;
/*!40000 ALTER TABLE `nsm_db_version` DISABLE KEYS */;
INSERT INTO `nsm_db_version` VALUES (1,'icinga-web/v1.11.2','2015-06-16 18:19:07','2015-06-16 18:19:07');
/*!40000 ALTER TABLE `nsm_db_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nsm_log`
--

DROP TABLE IF EXISTS `nsm_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nsm_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_level` int(11) NOT NULL,
  `log_message` text NOT NULL,
  `log_created` datetime NOT NULL,
  `log_modified` datetime NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nsm_log`
--

LOCK TABLES `nsm_log` WRITE;
/*!40000 ALTER TABLE `nsm_log` DISABLE KEYS */;
INSERT INTO `nsm_log` VALUES (1,2,'Userlogin by icinga failed! (ip=62.23.149.50)','2015-06-16 16:50:27','2015-06-16 16:50:27'),(2,2,'Userlogin by icinga failed! (ip=62.23.149.50)','2015-06-16 16:50:34','2015-06-16 16:50:34'),(3,2,'Userlogin by icinga_web failed! (ip=62.23.149.50)','2015-06-16 16:51:10','2015-06-16 16:51:10'),(4,2,'Userlogin by root failed! (ip=62.23.149.50)','2015-06-16 16:54:15','2015-06-16 16:54:15'),(5,2,'Userlogin by icinga_web failed! (ip=62.23.149.50)','2015-06-16 16:55:53','2015-06-16 16:55:53'),(6,2,'Userlogin by icinga_web failed! (ip=62.23.149.50)','2015-06-16 16:56:02','2015-06-16 16:56:02'),(7,2,'Userlogin by icinga failed! (ip=62.23.149.50)','2015-06-16 16:56:08','2015-06-16 16:56:08'),(8,4,'New: Setting org.icinga.ext.appstate => {\"upref_id\":\"1\",\"upref_user_id\":\"1\",\"upref_val\":null,\"upref_longval\":\"[{\\\"name\\\":\\\"cronk-listing-panel\\\",\\\"value\\\":\\\"{\\\\\\\"collapsed_categories\\\\\\\":[\\\\\\\"to\\\\\\\",\\\\\\\"misc\\\\\\\"],\\\\\\\"cronkliststyle\\\\\\\":\\\\\\\"list\\\\\\\",\\\\\\\"tabslider_active\\\\\\\":false}\\\"},{\\\"name\\\":\\\"cronk-tab-panel\\\",\\\"value\\\":\\\"{\\\\\\\"cronks\\\\\\\":{\\\\\\\"cr-panel-1434473847\\\\\\\":{\\\\\\\"params\\\\\\\":{\\\\\\\"module\\\\\\\":\\\\\\\"Cronks\\\\\\\",\\\\\\\"action\\\\\\\":\\\\\\\"System.PortalHello\\\\\\\"},\\\\\\\"crname\\\\\\\":\\\\\\\"portalHello\\\\\\\",\\\\\\\"autoRefresh\\\\\\\":true,\\\\\\\"cdata\\\\\\\":{},\\\\\\\"cenv\\\\\\\":{},\\\\\\\"autoLayout\\\\\\\":false,\\\\\\\"cmpid\\\\\\\":\\\\\\\"cronk-cid1434473849\\\\\\\",\\\\\\\"stateuid\\\\\\\":\\\\\\\"cronk-sid1434473848\\\\\\\",\\\\\\\"parentid\\\\\\\":\\\\\\\"cr-panel-1434473847\\\\\\\",\\\\\\\"title\\\\\\\":\\\\\\\"Welcome\\\\\\\",\\\\\\\"xtype\\\\\\\":\\\\\\\"cronk\\\\\\\",\\\\\\\"closable\\\\\\\":true,\\\\\\\"id\\\\\\\":\\\\\\\"cr-panel-1434473847\\\\\\\",\\\\\\\"border\\\\\\\":false,\\\\\\\"iconCls\\\\\\\":\\\\\\\"icinga-cronk-icon-start\\\\\\\"}},\\\\\\\"items\\\\\\\":1,\\\\\\\"active\\\\\\\":\\\\\\\"cr-panel-1434473847\\\\\\\",\\\\\\\"tabOrder\\\\\\\":[\\\\\\\"cr-panel-1434473847\\\\\\\"]}\\\"}]\",\"upref_key\":\"org.icinga.ext.appstate\",\"upref_created\":\"2015-06-16 16:57:20\",\"upref_modified\":\"2015-06-16 16:57:20\"} (NsmUser::setPref(), line 317)','2015-06-16 16:57:20','2015-06-16 16:57:20');
/*!40000 ALTER TABLE `nsm_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nsm_principal`
--

DROP TABLE IF EXISTS `nsm_principal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nsm_principal` (
  `principal_id` int(11) NOT NULL AUTO_INCREMENT,
  `principal_user_id` int(11) DEFAULT NULL,
  `principal_role_id` int(11) DEFAULT NULL,
  `principal_type` varchar(4) NOT NULL,
  `principal_disabled` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`principal_id`),
  KEY `principal_collection_idx_idx` (`principal_user_id`,`principal_role_id`,`principal_type`),
  KEY `principal_user_id_idx` (`principal_user_id`),
  KEY `principal_role_id_idx` (`principal_role_id`),
  CONSTRAINT `nsm_principal_principal_role_id_nsm_role_role_id` FOREIGN KEY (`principal_role_id`) REFERENCES `nsm_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nsm_principal_principal_user_id_nsm_user_user_id` FOREIGN KEY (`principal_user_id`) REFERENCES `nsm_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nsm_principal`
--

LOCK TABLES `nsm_principal` WRITE;
/*!40000 ALTER TABLE `nsm_principal` DISABLE KEYS */;
INSERT INTO `nsm_principal` VALUES (1,1,NULL,'user',0),(2,NULL,2,'role',0),(3,NULL,3,'role',0),(4,NULL,1,'role',0),(5,NULL,4,'role',0);
/*!40000 ALTER TABLE `nsm_principal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nsm_principal_target`
--

DROP TABLE IF EXISTS `nsm_principal_target`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nsm_principal_target` (
  `pt_id` int(11) NOT NULL AUTO_INCREMENT,
  `pt_principal_id` int(11) NOT NULL,
  `pt_target_id` int(11) NOT NULL,
  PRIMARY KEY (`pt_id`),
  KEY `pt_target_id_ix_idx` (`pt_target_id`),
  KEY `pt_principal_id_ix_idx` (`pt_principal_id`),
  CONSTRAINT `nsm_principal_target_pt_principal_id_nsm_principal_principal_id` FOREIGN KEY (`pt_principal_id`) REFERENCES `nsm_principal` (`principal_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nsm_principal_target_pt_target_id_nsm_target_target_id` FOREIGN KEY (`pt_target_id`) REFERENCES `nsm_target` (`target_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nsm_principal_target`
--

LOCK TABLES `nsm_principal_target` WRITE;
/*!40000 ALTER TABLE `nsm_principal_target` DISABLE KEYS */;
INSERT INTO `nsm_principal_target` VALUES (1,2,8),(2,2,13),(3,3,9),(4,3,10),(5,3,11),(6,4,8),(7,5,7),(8,3,15),(9,3,16),(10,3,17),(11,3,18),(12,4,20),(13,3,21);
/*!40000 ALTER TABLE `nsm_principal_target` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nsm_role`
--

DROP TABLE IF EXISTS `nsm_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nsm_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(40) NOT NULL,
  `role_description` varchar(255) DEFAULT NULL,
  `role_disabled` tinyint(4) NOT NULL DEFAULT '0',
  `role_created` datetime NOT NULL,
  `role_modified` datetime NOT NULL,
  `role_parent` int(11) DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  KEY `role_parent_idx` (`role_parent`),
  CONSTRAINT `nsm_role_role_parent_nsm_role_role_id` FOREIGN KEY (`role_parent`) REFERENCES `nsm_role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nsm_role`
--

LOCK TABLES `nsm_role` WRITE;
/*!40000 ALTER TABLE `nsm_role` DISABLE KEYS */;
INSERT INTO `nsm_role` VALUES (1,'icinga_user','The default representation of a ICINGA user',0,'2015-06-16 18:19:07','2015-06-16 18:19:07',NULL),(2,'appkit_user','Appkit user test',0,'2015-06-16 18:19:07','2015-06-16 18:19:07',NULL),(3,'appkit_admin','AppKit admin',0,'2015-06-16 18:19:07','2015-06-16 18:19:07',2),(4,'guest','Unauthorized Guest',0,'2015-06-16 18:19:07','2015-06-16 18:19:07',NULL);
/*!40000 ALTER TABLE `nsm_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nsm_session`
--

DROP TABLE IF EXISTS `nsm_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nsm_session` (
  `session_entry_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(255) NOT NULL,
  `session_name` varchar(255) NOT NULL,
  `session_data` longtext NOT NULL,
  `session_checksum` varchar(255) NOT NULL,
  `session_created` datetime NOT NULL,
  `session_modified` datetime NOT NULL,
  PRIMARY KEY (`session_entry_id`),
  UNIQUE KEY `session_id_idx` (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nsm_session`
--

LOCK TABLES `nsm_session` WRITE;
/*!40000 ALTER TABLE `nsm_session` DISABLE KEYS */;
INSERT INTO `nsm_session` VALUES (1,'ffr8j7nmqomo01coon5rp2s5b1','icinga-web','','d41d8cd98f00b204e9800998ecf8427e','2015-06-16 16:48:04','2015-06-16 16:48:04'),(2,'5osqjb3nfuvit4qqfhad57ou12','icinga-web','org.agavi.user.RbacSecurityUser.roles|a:4:{i:0;C:7:\"NsmRole\":627:{a:14:{s:3:\"_id\";a:1:{s:7:\"role_id\";s:1:\"1\";}s:5:\"_data\";a:7:{s:7:\"role_id\";s:1:\"1\";s:9:\"role_name\";s:11:\"icinga_user\";s:16:\"role_description\";s:43:\"The default representation of a ICINGA user\";s:13:\"role_disabled\";s:1:\"0\";s:12:\"role_created\";s:19:\"2015-06-16 18:19:07\";s:13:\"role_modified\";s:19:\"2015-06-16 18:19:07\";s:11:\"role_parent\";N;}s:7:\"_values\";a:0:{}s:6:\"_state\";i:3;s:13:\"_lastModified\";a:0:{}s:9:\"_modified\";a:0:{}s:10:\"_oldValues\";a:0:{}s:15:\"_pendingDeletes\";a:0:{}s:15:\"_pendingUnlinks\";a:0:{}s:20:\"_serializeReferences\";b:0;s:17:\"_invokedSaveHooks\";a:0:{}s:4:\"_oid\";i:5;s:8:\"_locator\";N;s:10:\"_resources\";a:0:{}}}i:1;C:7:\"NsmRole\":600:{a:14:{s:3:\"_id\";a:1:{s:7:\"role_id\";s:1:\"2\";}s:5:\"_data\";a:7:{s:7:\"role_id\";s:1:\"2\";s:9:\"role_name\";s:11:\"appkit_user\";s:16:\"role_description\";s:16:\"Appkit user test\";s:13:\"role_disabled\";s:1:\"0\";s:12:\"role_created\";s:19:\"2015-06-16 18:19:07\";s:13:\"role_modified\";s:19:\"2015-06-16 18:19:07\";s:11:\"role_parent\";N;}s:7:\"_values\";a:0:{}s:6:\"_state\";i:3;s:13:\"_lastModified\";a:0:{}s:9:\"_modified\";a:0:{}s:10:\"_oldValues\";a:0:{}s:15:\"_pendingDeletes\";a:0:{}s:15:\"_pendingUnlinks\";a:0:{}s:20:\"_serializeReferences\";b:0;s:17:\"_invokedSaveHooks\";a:0:{}s:4:\"_oid\";i:7;s:8:\"_locator\";N;s:10:\"_resources\";a:0:{}}}i:2;C:7:\"NsmRole\":603:{a:14:{s:3:\"_id\";a:1:{s:7:\"role_id\";s:1:\"3\";}s:5:\"_data\";a:7:{s:7:\"role_id\";s:1:\"3\";s:9:\"role_name\";s:12:\"appkit_admin\";s:16:\"role_description\";s:12:\"AppKit admin\";s:13:\"role_disabled\";s:1:\"0\";s:12:\"role_created\";s:19:\"2015-06-16 18:19:07\";s:13:\"role_modified\";s:19:\"2015-06-16 18:19:07\";s:11:\"role_parent\";s:1:\"2\";}s:7:\"_values\";a:0:{}s:6:\"_state\";i:3;s:13:\"_lastModified\";a:0:{}s:9:\"_modified\";a:0:{}s:10:\"_oldValues\";a:0:{}s:15:\"_pendingDeletes\";a:0:{}s:15:\"_pendingUnlinks\";a:0:{}s:20:\"_serializeReferences\";b:0;s:17:\"_invokedSaveHooks\";a:0:{}s:4:\"_oid\";i:9;s:8:\"_locator\";N;s:10:\"_resources\";a:0:{}}}i:3;r:26;}org.agavi.user.BasicSecurityUser.authenticated|b:1;org.agavi.user.BasicSecurityUser.credentials|a:11:{i:0;s:11:\"icinga.user\";i:1;s:19:\"icinga.cronk.custom\";i:2;s:17:\"appkit.api.access\";i:3;s:19:\"appkit.admin.groups\";i:4;s:18:\"appkit.admin.users\";i:5;s:12:\"appkit.admin\";i:6;s:27:\"icinga.cronk.category.admin\";i:7;s:16:\"icinga.cronk.log\";i:8;s:19:\"icinga.control.view\";i:9;s:20:\"icinga.control.admin\";i:10;s:18:\"icinga.cronk.admin\";}org.agavi.user.User|a:1:{s:9:\"org.agavi\";a:2:{s:7:\"userobj\";C:7:\"NsmUser\":1150:{a:14:{s:3:\"_id\";a:1:{s:7:\"user_id\";s:1:\"1\";}s:5:\"_data\";a:16:{s:7:\"user_id\";s:1:\"1\";s:12:\"user_account\";s:1:\"0\";s:9:\"user_name\";s:4:\"root\";s:13:\"user_lastname\";s:4:\"Root\";s:14:\"user_firstname\";s:5:\"Enoch\";s:13:\"user_password\";s:64:\"9da166fe7a0ab4c2b2c2709b5a61249f7d69c1c566cf59f70b1ab3a4ab7997f5\";s:9:\"user_salt\";s:64:\"cddc64ba9346d3ed61535634cf739d8c421aa9b37ac2ab04be2c8658fd6d8649\";s:12:\"user_authsrc\";s:8:\"internal\";s:11:\"user_authid\";N;s:12:\"user_authkey\";N;s:10:\"user_email\";s:20:\"root@localhost.local\";s:16:\"user_description\";N;s:13:\"user_disabled\";s:1:\"0\";s:12:\"user_created\";s:19:\"2015-06-16 18:19:07\";s:13:\"user_modified\";s:19:\"2015-06-17 08:51:19\";s:15:\"user_last_login\";s:19:\"2015-06-17 08:51:19\";}s:7:\"_values\";a:0:{}s:6:\"_state\";i:3;s:13:\"_lastModified\";a:2:{i:0;s:15:\"user_last_login\";i:1;s:13:\"user_modified\";}s:9:\"_modified\";a:0:{}s:10:\"_oldValues\";a:2:{s:15:\"user_last_login\";s:19:\"2015-06-16 04:57:16\";s:13:\"user_modified\";s:19:\"2015-06-16 16:57:16\";}s:15:\"_pendingDeletes\";a:0:{}s:15:\"_pendingUnlinks\";a:0:{}s:20:\"_serializeReferences\";b:0;s:17:\"_invokedSaveHooks\";a:0:{}s:4:\"_oid\";i:12;s:8:\"_locator\";N;s:10:\"_resources\";a:0:{}}}s:15:\"currentProvider\";s:8:\"internal\";}}icinga.cronks.cache.xml|a:2:{s:9:\"timestamp\";i:1434531080;s:4:\"data\";a:39:{s:19:\"icingaOverallStatus\";a:17:{s:7:\"cronkid\";s:19:\"icingaOverallStatus\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:20:\"System.StatusOverall\";s:4:\"hide\";b:1;s:11:\"description\";s:14:\"Overall Status\";s:4:\"name\";s:27:\"Displays the overall status\";s:10:\"categories\";s:4:\"core\";s:5:\"image\";s:13:\"cronks.Folder\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";N;s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";i:0;s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:24:\"icingaMonitorPerformance\";a:17:{s:7:\"cronkid\";s:24:\"icingaMonitorPerformance\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:25:\"System.MonitorPerformance\";s:4:\"hide\";b:1;s:11:\"description\";s:24:\"Monitor performance data\";s:4:\"name\";s:32:\"Display monitor performance data\";s:10:\"categories\";s:4:\"core\";s:5:\"image\";s:13:\"cronks.Folder\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:2:{s:30:\"serviceLatencyWarningThreshold\";s:5:\"10.00\";s:27:\"hostLatencyWarningThreshold\";s:5:\"10.00\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";i:0;s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:12:\"icingaToProc\";a:17:{s:7:\"cronkid\";s:12:\"icingaToProc\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:20:\"System.StaticContent\";s:4:\"hide\";b:0;s:11:\"description\";s:24:\"Icinga Tactical Overview\";s:4:\"name\";s:20:\"Generic TO processor\";s:10:\"categories\";s:4:\"core\";s:5:\"image\";s:13:\"cronks.Folder\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:9:\"interface\";b:1;}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";i:0;s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:8:\"gridProc\";a:17:{s:7:\"cronkid\";s:8:\"gridProc\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:1;s:11:\"description\";s:63:\"A generic ajax grid xml processor to display icinga information\";s:4:\"name\";N;s:10:\"categories\";s:4:\"core\";s:5:\"image\";s:13:\"cronks.Folder\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";N;s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";i:0;s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:8:\"crportal\";a:17:{s:7:\"cronkid\";s:8:\"crportal\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:18:\"System.CronkPortal\";s:4:\"hide\";b:1;s:11:\"description\";s:43:\"The cronkportal itself (icinga application)\";s:4:\"name\";s:12:\"Cronk portal\";s:10:\"categories\";s:4:\"core\";s:5:\"image\";s:13:\"cronks.Folder\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";N;s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";i:0;s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:6:\"crlist\";a:17:{s:7:\"cronkid\";s:6:\"crlist\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:19:\"System.CronkListing\";s:4:\"hide\";b:1;s:11:\"description\";s:22:\"Lists available cronks\";s:4:\"name\";s:23:\"Cronk dataview provider\";s:10:\"categories\";s:4:\"core\";s:5:\"image\";s:13:\"cronks.Folder\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";N;s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";i:0;s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:12:\"icingaSearch\";a:17:{s:7:\"cronkid\";s:12:\"icingaSearch\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:19:\"System.ObjectSearch\";s:4:\"hide\";b:1;s:11:\"description\";s:16:\"Search something\";s:4:\"name\";s:6:\"Search\";s:10:\"categories\";s:4:\"core\";s:5:\"image\";s:13:\"cronks.Folder\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";N;s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";i:0;s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:13:\"genericIFrame\";a:17:{s:7:\"cronkid\";s:13:\"genericIFrame\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:17:\"System.IframeView\";s:4:\"hide\";b:1;s:11:\"description\";s:31:\"View something within an iframe\";s:4:\"name\";s:6:\"Icinga\";s:10:\"categories\";s:4:\"misc\";s:5:\"image\";s:12:\"cronks.world\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";N;s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";i:0;s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:11:\"portalHello\";a:17:{s:7:\"cronkid\";s:11:\"portalHello\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:18:\"System.PortalHello\";s:4:\"hide\";b:1;s:11:\"description\";s:21:\"Welcome to Icinga Web\";s:4:\"name\";s:7:\"Welcome\";s:10:\"categories\";s:4:\"demo\";s:5:\"image\";s:12:\"cronks.start\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";N;s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";i:0;s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:20:\"gridHostgroupSummary\";a:17:{s:7:\"cronkid\";s:20:\"gridHostgroupSummary\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:25:\"Display hostgroup summary\";s:4:\"name\";s:10:\"Hostgroups\";s:10:\"categories\";s:6:\"status\";s:5:\"image\";s:11:\"cronks.Dots\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:33:\"icinga-hostgroup-summary-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"300\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:23:\"gridServicegroupSummary\";a:17:{s:7:\"cronkid\";s:23:\"gridServicegroupSummary\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:23:\"Display service summary\";s:4:\"name\";s:13:\"Servicegroups\";s:10:\"categories\";s:6:\"status\";s:5:\"image\";s:19:\"cronks.Applications\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:36:\"icinga-servicegroup-summary-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"310\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:12:\"gridHostView\";a:17:{s:7:\"cronkid\";s:12:\"gridHostView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:29:\"Viewing host status in a grid\";s:4:\"name\";s:10:\"HostStatus\";s:10:\"categories\";s:6:\"status\";s:5:\"image\";s:15:\"cronks.Computer\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:20:\"icinga-host-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"100\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:19:\"gridHostHistoryView\";a:17:{s:7:\"cronkid\";s:19:\"gridHostHistoryView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:31:\"Host status history information\";s:4:\"name\";s:11:\"HostHistory\";s:10:\"categories\";s:7:\"history\";s:5:\"image\";s:12:\"cronks.Stats\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:28:\"icinga-host-history-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"110\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:15:\"gridServiceView\";a:17:{s:7:\"cronkid\";s:15:\"gridServiceView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:32:\"Viewing service status in a grid\";s:4:\"name\";s:13:\"ServiceStatus\";s:10:\"categories\";s:6:\"status\";s:5:\"image\";s:13:\"cronks.Stats2\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:23:\"icinga-service-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:2:\"50\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:22:\"gridServiceHistoryView\";a:17:{s:7:\"cronkid\";s:22:\"gridServiceHistoryView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:26:\"Statushistory for services\";s:4:\"name\";s:14:\"ServiceHistory\";s:10:\"categories\";s:7:\"history\";s:5:\"image\";s:12:\"cronks.Stats\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:31:\"icinga-service-history-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:2:\"60\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:11:\"gridLogView\";a:17:{s:7:\"cronkid\";s:11:\"gridLogView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:26:\"Display icinga process log\";s:4:\"name\";s:7:\"LogView\";s:10:\"categories\";s:7:\"history\";s:5:\"image\";s:11:\"cronks.Info\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:19:\"icinga-log-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"500\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:20:\"gridOpenProblemsView\";a:17:{s:7:\"cronkid\";s:20:\"gridOpenProblemsView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:29:\"Show summary of open problems\";s:4:\"name\";s:13:\"Open problems\";s:10:\"categories\";s:8:\"problems\";s:5:\"image\";s:14:\"cronks.Warning\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:29:\"icinga-open-problems-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:2:\"30\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:19:\"gridAllProblemsView\";a:17:{s:7:\"cronkid\";s:19:\"gridAllProblemsView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:24:\"Show summary of problems\";s:4:\"name\";s:12:\"All problems\";s:10:\"categories\";s:8:\"problems\";s:5:\"image\";s:11:\"cronks.Tool\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:28:\"icinga-all-problems-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:2:\"31\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:29:\"gridUnhandledHostProblemsView\";a:17:{s:7:\"cronkid\";s:29:\"gridUnhandledHostProblemsView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:39:\"Show summary of unhandled host problems\";s:4:\"name\";s:23:\"Unhandled host problems\";s:10:\"categories\";s:8:\"problems\";s:5:\"image\";s:14:\"cronks.Warning\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:30:\"icinga-unhandled-host-problems\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:2:\"20\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:23:\"gridAllHostProblemsView\";a:17:{s:7:\"cronkid\";s:23:\"gridAllHostProblemsView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:22:\"Show all host problems\";s:4:\"name\";s:17:\"All host problems\";s:10:\"categories\";s:8:\"problems\";s:5:\"image\";s:11:\"cronks.Tool\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:24:\"icinga-all-host-problems\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:2:\"21\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:32:\"gridUnhandledServiceProblemsView\";a:17:{s:7:\"cronkid\";s:32:\"gridUnhandledServiceProblemsView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:42:\"Show summary of unhandled service problems\";s:4:\"name\";s:26:\"Unhandled service problems\";s:10:\"categories\";s:8:\"problems\";s:5:\"image\";s:14:\"cronks.Warning\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:33:\"icinga-unhandled-service-problems\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:2:\"10\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:26:\"gridAllServiceProblemsView\";a:17:{s:7:\"cronkid\";s:26:\"gridAllServiceProblemsView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:25:\"Show all service problems\";s:4:\"name\";s:20:\"All service problems\";s:10:\"categories\";s:8:\"problems\";s:5:\"image\";s:11:\"cronks.Tool\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:27:\"icinga-all-service-problems\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:2:\"11\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:20:\"gridNotificationView\";a:17:{s:7:\"cronkid\";s:20:\"gridNotificationView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:28:\"Display icinga notifications\";s:4:\"name\";s:13:\"Notifications\";s:10:\"categories\";s:7:\"history\";s:5:\"image\";s:15:\"cronks.Bubble_1\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:28:\"icinga-notification-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"410\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:16:\"gridDowntimeView\";a:17:{s:7:\"cronkid\";s:16:\"gridDowntimeView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:24:\"Display icinga downtimes\";s:4:\"name\";s:9:\"Downtimes\";s:10:\"categories\";s:6:\"status\";s:5:\"image\";s:12:\"cronks.Sleep\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:24:\"icinga-downtime-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"390\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:23:\"gridDowntimeHistoryView\";a:17:{s:7:\"cronkid\";s:23:\"gridDowntimeHistoryView\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:31:\"Display icinga downtime history\";s:4:\"name\";s:16:\"Downtime History\";s:10:\"categories\";s:7:\"history\";s:5:\"image\";s:12:\"cronks.Clock\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:32:\"icinga-downtime-history-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"400\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:18:\"gridInstanceStatus\";a:17:{s:7:\"cronkid\";s:18:\"gridInstanceStatus\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:15:\"System.ViewProc\";s:4:\"hide\";b:0;s:11:\"description\";s:25:\"Display instance overview\";s:4:\"name\";s:9:\"Instances\";s:10:\"categories\";s:6:\"status\";s:5:\"image\";s:11:\"cronks.Tree\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"template\";s:24:\"icinga-instance-template\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"520\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:15:\"icingaStatusMap\";a:17:{s:7:\"cronkid\";s:15:\"icingaStatusMap\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:16:\"System.StatusMap\";s:4:\"hide\";b:0;s:11:\"description\";s:10:\"Status Map\";s:4:\"name\";s:10:\"Status Map\";s:10:\"categories\";s:6:\"status\";s:5:\"image\";s:12:\"cronks.Graph\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";N;s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"420\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:14:\"portalView1Col\";a:17:{s:7:\"cronkid\";s:14:\"portalView1Col\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:17:\"System.PortalView\";s:4:\"hide\";b:0;s:11:\"description\";s:19:\"Portalgrid 1 column\";s:4:\"name\";s:14:\"Portal 1column\";s:10:\"categories\";s:4:\"misc\";s:5:\"image\";s:8:\"cronks.1\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:7:\"columns\";s:1:\"1\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"100\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:14:\"portalView2Col\";a:17:{s:7:\"cronkid\";s:14:\"portalView2Col\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:17:\"System.PortalView\";s:4:\"hide\";b:0;s:11:\"description\";s:19:\"Portalgrid 2 column\";s:4:\"name\";s:15:\"Portal 2columns\";s:10:\"categories\";s:4:\"misc\";s:5:\"image\";s:8:\"cronks.2\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:7:\"columns\";s:1:\"2\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"110\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:14:\"portalView3Col\";a:17:{s:7:\"cronkid\";s:14:\"portalView3Col\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:17:\"System.PortalView\";s:4:\"hide\";b:0;s:11:\"description\";s:19:\"Portalgrid 3 column\";s:4:\"name\";s:15:\"Portal 3columns\";s:10:\"categories\";s:4:\"misc\";s:5:\"image\";s:8:\"cronks.3\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:7:\"columns\";s:1:\"3\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"120\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:16:\"iframeViewIcinga\";a:17:{s:7:\"cronkid\";s:16:\"iframeViewIcinga\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:17:\"System.IframeView\";s:4:\"hide\";b:0;s:11:\"description\";s:34:\"View icinga classic on same server\";s:4:\"name\";s:14:\"Icinga Classic\";s:10:\"categories\";s:4:\"misc\";s:5:\"image\";s:12:\"cronks.Globe\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:3:{s:3:\"url\";s:8:\"/icinga/\";s:4:\"user\";s:5:\"guest\";s:8:\"password\";s:5:\"guest\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"200\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:22:\"iframeViewIcingaDocsEn\";a:17:{s:7:\"cronkid\";s:22:\"iframeViewIcingaDocsEn\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:17:\"System.IframeView\";s:4:\"hide\";b:0;s:11:\"description\";s:27:\"Icinga docs english version\";s:4:\"name\";s:7:\"Docs EN\";s:10:\"categories\";s:4:\"misc\";s:5:\"image\";s:12:\"cronks.Info2\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:3:\"url\";s:30:\"/icinga-web/docs/en/index.html\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"300\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:22:\"iframeViewIcingaDocsDe\";a:17:{s:7:\"cronkid\";s:22:\"iframeViewIcingaDocsDe\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:17:\"System.IframeView\";s:4:\"hide\";b:0;s:11:\"description\";s:26:\"Icinga docs german version\";s:4:\"name\";s:7:\"Docs DE\";s:10:\"categories\";s:4:\"misc\";s:5:\"image\";s:12:\"cronks.Info2\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:3:\"url\";s:30:\"/icinga-web/docs/de/index.html\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"310\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:28:\"icingaExampleCronkHelloWorld\";a:17:{s:7:\"cronkid\";s:28:\"icingaExampleCronkHelloWorld\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:18:\"Example.HelloWorld\";s:4:\"hide\";b:1;s:11:\"description\";s:27:\"Just say hello to the world\";s:4:\"name\";s:10:\"HelloWorld\";s:10:\"categories\";s:4:\"misc\";s:5:\"image\";s:13:\"cronks.Folder\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:1:{s:8:\"say_what\";s:17:\"Hello icinga-web!\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";i:0;s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:24:\"icingaTacticalOverviewHG\";a:17:{s:7:\"cronkid\";s:24:\"icingaTacticalOverviewHG\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:20:\"System.StaticContent\";s:4:\"hide\";b:0;s:11:\"description\";s:24:\"Icinga Tactical Overview\";s:4:\"name\";s:12:\"TO Hostgroup\";s:10:\"categories\";s:2:\"to\";s:5:\"image\";s:11:\"cronks.Dots\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:2:{s:8:\"template\";s:36:\"icinga-tactical-overview-template-hg\";s:9:\"interface\";b:1;}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"200\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:33:\"icingaTacticalOverviewChartSumAll\";a:17:{s:7:\"cronkid\";s:33:\"icingaTacticalOverviewChartSumAll\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:20:\"System.StaticContent\";s:4:\"hide\";b:0;s:11:\"description\";s:24:\"Icinga Tactical Overview\";s:4:\"name\";s:9:\"TO Charts\";s:10:\"categories\";s:2:\"to\";s:5:\"image\";s:12:\"cronks.Stats\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:2:{s:8:\"template\";s:40:\"icinga-tactical-overview-template-charts\";s:9:\"interface\";b:1;}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"100\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:24:\"icingaTacticalOverviewCV\";a:17:{s:7:\"cronkid\";s:24:\"icingaTacticalOverviewCV\";s:6:\"module\";s:6:\"Cronks\";s:6:\"action\";s:20:\"System.StaticContent\";s:4:\"hide\";b:0;s:11:\"description\";s:24:\"Icinga Tactical Overview\";s:4:\"name\";s:17:\"TO CustomVariable\";s:10:\"categories\";s:2:\"to\";s:5:\"image\";s:10:\"cronks.Tag\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";N;s:5:\"state\";N;s:12:\"ae:parameter\";a:2:{s:8:\"template\";s:36:\"icinga-tactical-overview-template-cv\";s:9:\"interface\";b:1;}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"300\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:22:\"icingaReportingDefault\";a:17:{s:7:\"cronkid\";s:22:\"icingaReportingDefault\";s:6:\"module\";s:9:\"Reporting\";s:6:\"action\";s:10:\"Cronk.Main\";s:4:\"hide\";b:1;s:11:\"description\";s:27:\"Seamless Jasper Integration\";s:4:\"name\";s:9:\"Reporting\";s:10:\"categories\";s:6:\"addons\";s:5:\"image\";s:24:\"cronks.Weather_Could_Sun\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";s:12:\"appkit_admin\";s:5:\"state\";N;s:12:\"ae:parameter\";a:4:{s:12:\"jasperconfig\";s:38:\"modules.reporting.jasperconfig.default\";s:15:\"enable_onthefly\";s:1:\"1\";s:17:\"enable_repository\";s:1:\"1\";s:17:\"enable_scheduling\";s:1:\"1\";}s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";i:0;s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}s:16:\"icingaConfigTree\";a:17:{s:7:\"cronkid\";s:16:\"icingaConfigTree\";s:6:\"module\";s:6:\"Config\";s:6:\"action\";s:17:\"ShowConfiguration\";s:4:\"hide\";b:0;s:11:\"description\";s:27:\"Configuration of icinga-web\";s:4:\"name\";s:13:\"Configuration\";s:10:\"categories\";s:4:\"misc\";s:5:\"image\";s:15:\"cronks.Database\";s:8:\"disabled\";b:0;s:10:\"groupsonly\";s:12:\"appkit_admin\";s:5:\"state\";N;s:12:\"ae:parameter\";N;s:6:\"system\";b:1;s:5:\"owner\";b:0;s:8:\"position\";s:3:\"500\";s:10:\"owner_name\";s:6:\"System\";s:8:\"owner_id\";i:0;}}}icinga.cronks.template<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>\n|s:17572:\"C:26:\"CronkGridTemplateXmlParser\":17530:{a:2:{s:4:\"data\";a:6:{s:4:\"type\";s:3:\"DQL\";s:4:\"meta\";a:3:{s:4:\"name\";s:39:\"Icinga services with unhandled problems\";s:11:\"description\";s:34:\"Displays object status information\";s:6:\"author\";s:21:\"icinga-web developers\";}s:6:\"option\";a:7:{s:4:\"mode\";s:7:\"default\";s:6:\"layout\";s:31:\"CronkGridTemplateAjaxGridLayout\";s:10:\"gridEvents\";a:2:{i:0;a:3:{s:4:\"type\";s:9:\"viewready\";s:8:\"function\";s:37:\"Cronk.grid.CommentColumnRenderer.init\";s:9:\"arguments\";a:2:{s:11:\"column_name\";s:8:\"comments\";s:5:\"title\";s:27:\"Comments for {service_name}\";}}i:1;a:4:{s:4:\"type\";s:9:\"viewready\";s:9:\"namespace\";s:33:\"Cronk.grid.InfoIconColumnRenderer\";s:8:\"function\";s:4:\"init\";s:9:\"arguments\";a:1:{s:11:\"column_name\";s:18:\"service_info_icons\";}}}s:9:\"rowEvents\";a:1:{i:0;a:3:{s:5:\"title\";s:6:\"Icinga\";s:6:\"menuid\";s:6:\"icinga\";s:5:\"items\";a:4:{i:0;a:9:{s:6:\"target\";s:3:\"sub\";s:7:\"handler\";a:1:{s:5:\"click\";s:31:\"Cronk.grid.handler.Info.service\";}s:16:\"handlerArguments\";a:1:{s:14:\"objectid_field\";s:17:\"service_object_id\";}s:5:\"model\";s:0:\"\";s:5:\"xtype\";s:15:\"grideventbutton\";s:6:\"menuid\";s:19:\"detail_info_service\";s:7:\"iconCls\";s:25:\"icinga-icon-servicedetail\";s:7:\"tooltip\";s:20:\"Detailed information\";s:4:\"text\";s:7:\"Details\";}i:1;a:9:{s:6:\"target\";s:3:\"sub\";s:7:\"handler\";a:1:{s:5:\"click\";s:28:\"Cronk.grid.handler.Info.host\";}s:16:\"handlerArguments\";a:1:{s:14:\"objectid_field\";s:14:\"host_object_id\";}s:5:\"model\";s:0:\"\";s:5:\"xtype\";s:15:\"grideventbutton\";s:6:\"menuid\";s:16:\"detail_info_host\";s:7:\"iconCls\";s:22:\"icinga-icon-hostdetail\";s:7:\"tooltip\";s:25:\"Detailed host information\";s:4:\"text\";s:12:\"Host details\";}i:2;a:9:{s:6:\"target\";s:3:\"sub\";s:7:\"handler\";a:1:{s:5:\"click\";s:40:\"Cronk.grid.handler.Grid.openTemplateGrid\";}s:16:\"handlerArguments\";a:5:{s:8:\"template\";s:20:\"icinga-host-template\";s:10:\"labelField\";s:12:\"service_name\";s:8:\"idPrefix\";s:18:\"host_subgrid_hosts\";s:11:\"titlePrefix\";s:9:\"Hosts for\";s:9:\"filterMap\";a:1:{s:14:\"host_object_id\";s:14:\"host_object_id\";}}s:5:\"model\";s:0:\"\";s:5:\"xtype\";s:15:\"grideventbutton\";s:6:\"menuid\";s:17:\"services_for_host\";s:7:\"iconCls\";s:16:\"icinga-icon-host\";s:7:\"tooltip\";s:9:\"Show host\";s:4:\"text\";s:4:\"Host\";}i:3;a:9:{s:6:\"target\";s:3:\"sub\";s:7:\"handler\";a:1:{s:5:\"click\";s:40:\"Cronk.grid.handler.Grid.openTemplateGrid\";}s:16:\"handlerArguments\";a:5:{s:8:\"template\";s:31:\"icinga-service-history-template\";s:10:\"labelField\";s:12:\"service_name\";s:8:\"idPrefix\";s:30:\"servicehistory_subgrid_service\";s:11:\"titlePrefix\";s:11:\"History for\";s:9:\"filterMap\";a:1:{s:17:\"service_object_id\";s:17:\"service_object_id\";}}s:5:\"model\";s:0:\"\";s:5:\"xtype\";s:15:\"grideventbutton\";s:6:\"menuid\";s:21:\"host_history_for_host\";s:7:\"iconCls\";s:16:\"icinga-icon-book\";s:7:\"tooltip\";s:12:\"Show history\";s:4:\"text\";s:7:\"History\";}}}}s:15:\"selection_model\";s:8:\"checkbox\";s:8:\"commands\";a:4:{s:7:\"enabled\";b:1;s:6:\"source\";a:3:{s:4:\"host\";s:9:\"host_name\";s:7:\"service\";s:12:\"service_name\";s:8:\"instance\";s:13:\"instance_name\";}s:10:\"predefined\";a:3:{s:6:\"author\";s:4:\"root\";s:7:\"endtime\";s:8:\"now+7200\";s:5:\"fixed\";d:1;}s:5:\"items\";a:21:{s:18:\"SCHEDULE_SVC_CHECK\";a:2:{s:5:\"title\";s:27:\"Schedule next service check\";s:10:\"icon_class\";s:15:\"icinga-icon-cog\";}s:25:\"SCHEDULE_FORCED_SVC_CHECK\";a:2:{s:5:\"title\";s:29:\"Schedule forced service check\";s:10:\"icon_class\";s:15:\"icinga-icon-cog\";}s:28:\"PROCESS_SERVICE_CHECK_RESULT\";a:2:{s:5:\"title\";s:28:\"Process service check result\";s:10:\"icon_class\";s:15:\"icinga-icon-cog\";}s:23:\"ACKNOWLEDGE_SVC_PROBLEM\";a:2:{s:5:\"title\";s:33:\"Acknowledge service check problem\";s:10:\"icon_class\";s:29:\"icinga-icon-exclamation-white\";}s:30:\"ACKNOWLEDGE_SVC_PROBLEM_EXPIRE\";a:2:{s:5:\"title\";s:54:\"Acknowledge service check problem with expiration time\";s:10:\"icon_class\";s:29:\"icinga-icon-exclamation-clock\";}s:26:\"REMOVE_SVC_ACKNOWLEDGEMENT\";a:2:{s:5:\"title\";s:30:\"Remove service acknowledgement\";s:10:\"icon_class\";s:27:\"icinga-icon-exclamation-red\";}s:21:\"SCHEDULE_SVC_DOWNTIME\";a:2:{s:5:\"title\";s:27:\"Schedule a service downtime\";s:10:\"icon_class\";s:20:\"icinga-icon-downtime\";}s:15:\"ADD_SVC_COMMENT\";a:2:{s:5:\"title\";s:21:\"Add a service comment\";s:10:\"icon_class\";s:19:\"icinga-icon-comment\";}s:28:\"SEND_CUSTOM_SVC_NOTIFICATION\";a:3:{s:5:\"title\";s:32:\"Send custom service notification\";s:10:\"icon_class\";s:18:\"icinga-icon-notify\";s:9:\"seperator\";b:1;}s:16:\"ENABLE_SVC_CHECK\";a:2:{s:5:\"title\";s:36:\"Enable active checks of this service\";s:10:\"icon_class\";s:21:\"icinga-icon-arrow-out\";}s:17:\"DISABLE_SVC_CHECK\";a:3:{s:5:\"title\";s:37:\"Disable active checks of this service\";s:10:\"icon_class\";s:17:\"icinga-icon-cross\";s:9:\"seperator\";b:1;}s:25:\"ENABLE_PASSIVE_SVC_CHECKS\";a:2:{s:5:\"title\";s:47:\"Start accepting passive checks for this service\";s:10:\"icon_class\";s:20:\"icinga-icon-arrow-in\";}s:26:\"DISABLE_PASSIVE_SVC_CHECKS\";a:3:{s:5:\"title\";s:46:\"Stop accepting passive checks for this service\";s:10:\"icon_class\";s:17:\"icinga-icon-cross\";s:9:\"seperator\";b:1;}s:24:\"START_OBSESSING_OVER_SVC\";a:2:{s:5:\"title\";s:33:\"Start obsessing over this service\";s:10:\"icon_class\";s:18:\"icinga-icon-bricks\";}s:23:\"STOP_OBSESSING_OVER_SVC\";a:3:{s:5:\"title\";s:32:\"Stop obsessing over this service\";s:10:\"icon_class\";s:17:\"icinga-icon-cross\";s:9:\"seperator\";b:1;}s:24:\"ENABLE_SVC_NOTIFICATIONS\";a:2:{s:5:\"title\";s:37:\"Enable notifications for this service\";s:10:\"icon_class\";s:18:\"icinga-icon-notify\";}s:25:\"DISABLE_SVC_NOTIFICATIONS\";a:3:{s:5:\"title\";s:38:\"Disable notifications for this service\";s:10:\"icon_class\";s:27:\"icinga-icon-notify-disabled\";s:9:\"seperator\";b:1;}s:24:\"ENABLE_SVC_EVENT_HANDLER\";a:2:{s:5:\"title\";s:37:\"Enable event handler for this service\";s:10:\"icon_class\";s:18:\"icinga-icon-bricks\";}s:25:\"DISABLE_SVC_EVENT_HANDLER\";a:3:{s:5:\"title\";s:38:\"Disable event handler for this service\";s:10:\"icon_class\";s:17:\"icinga-icon-cross\";s:9:\"seperator\";b:1;}s:25:\"ENABLE_SVC_FLAP_DETECTION\";a:2:{s:5:\"title\";s:38:\"Enable flap detection for this service\";s:10:\"icon_class\";s:20:\"icinga-icon-flapping\";}s:26:\"DISABLE_SVC_FLAP_DETECTION\";a:2:{s:5:\"title\";s:39:\"Disable flap detection for this service\";s:10:\"icon_class\";s:17:\"icinga-icon-cross\";}}}s:6:\"filter\";a:8:{s:19:\"customvariable_name\";a:8:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:21:\"appkit.ext.filter.api\";s:5:\"label\";s:7:\"CV Name\";s:13:\"operator_type\";s:4:\"text\";s:10:\"api_target\";s:14:\"customvariable\";s:12:\"api_keyfield\";s:19:\"CUSTOMVARIABLE_NAME\";s:14:\"api_valuefield\";s:19:\"CUSTOMVARIABLE_NAME\";}s:20:\"customvariable_value\";a:11:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:21:\"appkit.ext.filter.api\";s:5:\"label\";s:6:\"CV Val\";s:13:\"operator_type\";s:4:\"text\";s:10:\"api_target\";s:14:\"customvariable\";s:12:\"api_keyfield\";s:20:\"CUSTOMVARIABLE_VALUE\";s:14:\"api_valuefield\";s:20:\"CUSTOMVARIABLE_VALUE\";s:14:\"api_additional\";s:19:\"CUSTOMVARIABLE_NAME\";s:10:\"api_exttpl\";s:45:\"{CUSTOMVARIABLE_NAME}: {CUSTOMVARIABLE_VALUE}\";s:6:\"api_id\";s:20:\"CUSTOMVARIABLE_VALUE\";}s:14:\"hostgroup_name\";a:8:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:21:\"appkit.ext.filter.api\";s:5:\"label\";s:9:\"Hostgroup\";s:13:\"operator_type\";s:4:\"text\";s:10:\"api_target\";s:9:\"hostgroup\";s:12:\"api_keyfield\";s:14:\"HOSTGROUP_NAME\";s:14:\"api_valuefield\";s:14:\"HOSTGROUP_NAME\";}s:17:\"servicegroup_name\";a:8:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:21:\"appkit.ext.filter.api\";s:5:\"label\";s:12:\"Servicegroup\";s:13:\"operator_type\";s:4:\"text\";s:10:\"api_target\";s:12:\"servicegroup\";s:12:\"api_keyfield\";s:17:\"SERVICEGROUP_NAME\";s:14:\"api_valuefield\";s:17:\"SERVICEGROUP_NAME\";}s:21:\"notifications_enabled\";a:4:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:22:\"appkit.ext.filter.bool\";s:5:\"label\";s:21:\"Notifications enabled\";}s:20:\"problem_acknowledged\";a:4:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:22:\"appkit.ext.filter.bool\";s:5:\"label\";s:12:\"Acknowledged\";}s:24:\"scheduled_downtime_depth\";a:4:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:22:\"appkit.ext.filter.bool\";s:5:\"label\";s:11:\"In downtime\";}s:18:\"service_is_pending\";a:4:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:22:\"appkit.ext.filter.bool\";s:5:\"label\";s:10:\"Is pending\";}}}s:10:\"datasource\";a:3:{s:6:\"target\";s:28:\"TARGET_SERVICE_OPEN_PROBLEMS\";s:2:\"id\";s:17:\"service_object_id\";s:9:\"countmode\";s:5:\"field\";}s:5:\"pager\";a:2:{s:7:\"enabled\";b:1;s:5:\"start\";d:0;}s:8:\"grouping\";a:3:{s:7:\"enabled\";b:1;s:5:\"field\";s:9:\"host_name\";s:21:\"Ext.grid.GroupingView\";a:1:{s:17:\"hideGroupedColumn\";b:1;}}}s:6:\"fields\";a:18:{s:12:\"service_icon\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:18:\"SERVICE_ICON_IMAGE\";}s:7:\"display\";a:6:{s:7:\"visible\";b:1;s:5:\"label\";s:12:\"Object image\";s:4:\"icon\";s:17:\"icinga-icon-image\";s:5:\"width\";d:25;s:15:\"Ext.grid.Column\";a:2:{s:12:\"menuDisabled\";b:1;s:5:\"fixed\";b:1;}s:6:\"jsFunc\";a:1:{i:0;a:4:{s:9:\"namespace\";s:25:\"Cronk.grid.ColumnRenderer\";s:8:\"function\";s:11:\"columnImage\";s:4:\"type\";s:8:\"renderer\";s:9:\"arguments\";a:1:{s:5:\"image\";s:331:\"<tpl if=\"service_icon == \'\'\" />\n                                        images/icons/application-monitor.png\n                                    </tpl>\n                                    <tpl if=\"service_icon != \'\'\" />\n                                        images/icinga/{service_icon}\n                                    </tpl>\";}}}}s:6:\"filter\";a:1:{s:7:\"enabled\";b:0;}s:5:\"order\";a:2:{s:7:\"enabled\";b:0;s:7:\"default\";b:0;}}s:13:\"instance_name\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:13:\"INSTANCE_NAME\";}s:7:\"display\";a:4:{s:7:\"visible\";b:0;s:5:\"label\";s:8:\"Instance\";s:5:\"width\";d:60;s:15:\"Ext.grid.Column\";a:1:{s:5:\"fixed\";b:0;}}s:6:\"filter\";a:7:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:21:\"appkit.ext.filter.api\";s:13:\"operator_type\";s:4:\"text\";s:10:\"api_target\";s:8:\"instance\";s:12:\"api_keyfield\";s:13:\"INSTANCE_NAME\";s:14:\"api_valuefield\";s:13:\"INSTANCE_NAME\";}s:5:\"order\";a:2:{s:7:\"enabled\";b:0;s:7:\"default\";b:0;}}s:14:\"host_object_id\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:14:\"HOST_OBJECT_ID\";}s:7:\"display\";a:2:{s:7:\"visible\";b:0;s:5:\"label\";s:6:\"HostId\";}s:6:\"filter\";a:3:{s:7:\"enabled\";b:0;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:24:\"appkit.ext.filter.number\";}s:5:\"order\";a:3:{s:7:\"enabled\";b:1;s:7:\"default\";b:0;s:9:\"direction\";s:3:\"ASC\";}}s:17:\"service_object_id\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:17:\"SERVICE_OBJECT_ID\";}s:7:\"display\";a:2:{s:7:\"visible\";b:0;s:5:\"label\";s:2:\"Id\";}s:6:\"filter\";a:5:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:24:\"appkit.ext.filter.number\";s:11:\"no_operator\";b:0;s:5:\"field\";s:17:\"SERVICE_OBJECT_ID\";}s:5:\"order\";a:2:{s:7:\"enabled\";b:0;s:7:\"default\";b:0;}}s:9:\"host_name\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:9:\"HOST_NAME\";}s:7:\"display\";a:2:{s:7:\"visible\";b:1;s:5:\"label\";s:4:\"Host\";}s:6:\"filter\";a:7:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:21:\"appkit.ext.filter.api\";s:13:\"operator_type\";s:4:\"text\";s:10:\"api_target\";s:4:\"host\";s:12:\"api_keyfield\";s:9:\"HOST_NAME\";s:14:\"api_valuefield\";s:9:\"HOST_NAME\";}s:5:\"order\";a:3:{s:7:\"enabled\";b:1;s:7:\"default\";b:1;s:9:\"direction\";s:3:\"ASC\";}}s:10:\"host_alias\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:10:\"HOST_ALIAS\";}s:7:\"display\";a:2:{s:7:\"visible\";b:0;s:5:\"label\";s:10:\"Host alias\";}s:6:\"filter\";a:7:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:21:\"appkit.ext.filter.api\";s:13:\"operator_type\";s:4:\"text\";s:10:\"api_target\";s:4:\"host\";s:12:\"api_keyfield\";s:10:\"HOST_ALIAS\";s:14:\"api_valuefield\";s:10:\"HOST_ALIAS\";}s:5:\"order\";a:3:{s:7:\"enabled\";b:1;s:7:\"default\";b:0;s:9:\"direction\";s:3:\"ASC\";}}s:17:\"host_display_name\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:17:\"HOST_DISPLAY_NAME\";}s:7:\"display\";a:2:{s:7:\"visible\";b:0;s:5:\"label\";s:17:\"Host display name\";}s:6:\"filter\";a:7:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:21:\"appkit.ext.filter.api\";s:13:\"operator_type\";s:4:\"text\";s:10:\"api_target\";s:4:\"host\";s:12:\"api_keyfield\";s:17:\"HOST_DISPLAY_NAME\";s:14:\"api_valuefield\";s:17:\"HOST_DISPLAY_NAME\";}s:5:\"order\";a:3:{s:7:\"enabled\";b:1;s:7:\"default\";b:0;s:9:\"direction\";s:3:\"ASC\";}}s:12:\"service_name\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:12:\"SERVICE_NAME\";}s:7:\"display\";a:3:{s:7:\"visible\";b:1;s:5:\"label\";s:7:\"Service\";s:5:\"width\";d:180;}s:6:\"filter\";a:7:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:21:\"appkit.ext.filter.api\";s:13:\"operator_type\";s:4:\"text\";s:10:\"api_target\";s:7:\"service\";s:12:\"api_keyfield\";s:12:\"SERVICE_NAME\";s:14:\"api_valuefield\";s:12:\"SERVICE_NAME\";}s:5:\"order\";a:1:{s:7:\"enabled\";b:1;}}s:20:\"service_display_name\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:20:\"SERVICE_DISPLAY_NAME\";}s:7:\"display\";a:2:{s:7:\"visible\";b:0;s:5:\"label\";s:12:\"Display Name\";}s:6:\"filter\";a:7:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:21:\"appkit.ext.filter.api\";s:13:\"operator_type\";s:4:\"text\";s:10:\"api_target\";s:7:\"service\";s:12:\"api_keyfield\";s:20:\"SERVICE_DISPLAY_NAME\";s:14:\"api_valuefield\";s:20:\"SERVICE_DISPLAY_NAME\";}s:5:\"order\";a:1:{s:7:\"enabled\";b:1;}}s:8:\"comments\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:17:\"SERVICE_OBJECT_ID\";}s:7:\"display\";a:6:{s:7:\"visible\";b:1;s:5:\"label\";s:8:\"Comments\";s:4:\"icon\";s:19:\"icinga-icon-comment\";s:5:\"width\";d:22;s:15:\"Ext.grid.Column\";a:2:{s:12:\"menuDisabled\";b:1;s:5:\"fixed\";b:1;}s:6:\"jsFunc\";a:1:{i:0;a:4:{s:9:\"namespace\";s:32:\"Cronk.grid.CommentColumnRenderer\";s:8:\"function\";s:13:\"commentColumn\";s:4:\"type\";s:8:\"renderer\";s:9:\"arguments\";a:2:{s:6:\"target\";s:7:\"service\";s:12:\"target_field\";s:17:\"service_object_id\";}}}}s:6:\"filter\";a:1:{s:7:\"enabled\";b:0;}s:5:\"order\";a:1:{s:7:\"enabled\";b:0;}}s:18:\"service_state_type\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:18:\"SERVICE_STATE_TYPE\";}s:7:\"display\";a:5:{s:7:\"visible\";b:1;s:5:\"label\";s:10:\"State Type\";s:5:\"width\";d:60;s:15:\"Ext.grid.Column\";a:1:{s:5:\"fixed\";b:1;}s:6:\"jsFunc\";a:3:{s:9:\"namespace\";s:25:\"Cronk.grid.ColumnRenderer\";s:8:\"function\";s:9:\"stateType\";s:4:\"type\";s:8:\"renderer\";}}s:6:\"filter\";a:5:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:27:\"appkit.ext.filter.statetype\";s:11:\"no_operator\";b:0;s:5:\"field\";s:18:\"service_state_type\";}s:5:\"order\";a:1:{s:7:\"enabled\";b:1;}}s:14:\"service_status\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:21:\"SERVICE_CURRENT_STATE\";}s:7:\"display\";a:5:{s:7:\"visible\";b:1;s:5:\"label\";s:6:\"Status\";s:5:\"width\";d:100;s:15:\"Ext.grid.Column\";a:1:{s:5:\"fixed\";b:1;}s:6:\"jsFunc\";a:3:{s:9:\"namespace\";s:25:\"Cronk.grid.ColumnRenderer\";s:8:\"function\";s:13:\"serviceStatus\";s:4:\"type\";s:8:\"renderer\";}}s:6:\"filter\";a:4:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:31:\"appkit.ext.filter.servicestatus\";s:11:\"no_operator\";b:0;}s:5:\"order\";a:3:{s:7:\"enabled\";b:1;s:7:\"default\";b:0;s:5:\"field\";s:21:\"SERVICE_CURRENT_STATE\";}}s:18:\"service_last_check\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:18:\"SERVICE_LAST_CHECK\";}s:7:\"display\";a:4:{s:7:\"visible\";b:1;s:5:\"label\";s:10:\"Last check\";s:5:\"width\";d:120;s:8:\"userFunc\";a:2:{s:5:\"model\";s:27:\"Cronks.ColumnDisplay.Format\";s:6:\"method\";s:15:\"agaviDateFormat\";}}s:6:\"filter\";a:1:{s:7:\"enabled\";b:0;}s:5:\"order\";a:3:{s:7:\"enabled\";b:1;s:7:\"default\";b:1;s:5:\"order\";s:4:\"DESC\";}}s:16:\"service_duration\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:112:\"(CASE WHEN ss.last_state_change<=\'1970-01-01 00:00:00\' THEN ps.program_start_time ELSE ss.last_state_change END)\";}s:7:\"display\";a:4:{s:7:\"visible\";b:1;s:5:\"label\";s:8:\"Duration\";s:5:\"width\";d:110;s:6:\"jsFunc\";a:4:{s:9:\"namespace\";s:25:\"Cronk.grid.ColumnRenderer\";s:8:\"function\";s:13:\"durationField\";s:4:\"type\";s:8:\"renderer\";s:9:\"arguments\";a:1:{s:4:\"type\";s:7:\"service\";}}}s:6:\"filter\";a:1:{s:7:\"enabled\";b:0;}s:5:\"order\";a:3:{s:7:\"enabled\";b:1;s:7:\"default\";b:0;s:5:\"order\";s:4:\"DESC\";}}s:18:\"service_info_icons\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:17:\"SERVICE_OBJECT_ID\";}s:7:\"display\";a:5:{s:7:\"visible\";b:1;s:5:\"label\";s:4:\"Info\";s:5:\"width\";d:50;s:15:\"Ext.grid.Column\";a:1:{s:12:\"menuDisabled\";b:1;}s:6:\"jsFunc\";a:4:{s:9:\"namespace\";s:33:\"Cronk.grid.InfoIconColumnRenderer\";s:8:\"function\";s:10:\"infoColumn\";s:4:\"type\";s:8:\"renderer\";s:9:\"arguments\";a:1:{s:4:\"type\";d:2;}}}s:6:\"filter\";a:1:{s:7:\"enabled\";b:0;}s:5:\"order\";a:2:{s:7:\"enabled\";b:0;s:7:\"default\";b:0;}}s:13:\"plugin_output\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:14:\"SERVICE_OUTPUT\";}s:7:\"display\";a:4:{s:7:\"visible\";b:1;s:5:\"label\";s:6:\"Output\";s:5:\"width\";d:250;s:6:\"jsFunc\";a:1:{i:0;a:2:{s:9:\"namespace\";s:25:\"Cronk.grid.ColumnRenderer\";s:8:\"function\";s:29:\"customColumnPerfdataSanitized\";}}}s:6:\"filter\";a:1:{s:7:\"enabled\";b:0;}s:5:\"order\";a:2:{s:7:\"enabled\";b:0;s:7:\"default\";b:0;}}s:21:\"service_check_attempt\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:29:\"SERVICE_CURRENT_CHECK_ATTEMPT\";}s:7:\"display\";a:5:{s:7:\"visible\";b:1;s:5:\"label\";s:7:\"Attempt\";s:5:\"width\";d:60;s:15:\"Ext.grid.Column\";a:1:{s:12:\"menuDisabled\";b:1;}s:8:\"userFunc\";a:3:{s:5:\"model\";s:27:\"Cronks.ColumnDisplay.Format\";s:6:\"method\";s:14:\"formatTemplate\";s:9:\"arguments\";a:1:{s:6:\"format\";s:61:\"${field.service_check_attempt} / ${field.service_max_attempt}\";}}}s:6:\"filter\";a:1:{s:7:\"enabled\";b:0;}s:5:\"order\";a:2:{s:7:\"enabled\";b:1;s:7:\"default\";b:0;}}s:19:\"service_hard_status\";a:4:{s:10:\"datasource\";a:1:{s:5:\"field\";s:18:\"SERVICE_HARD_STATE\";}s:7:\"display\";a:5:{s:7:\"visible\";b:0;s:5:\"label\";s:18:\"Hard service state\";s:5:\"width\";d:100;s:15:\"Ext.grid.Column\";a:1:{s:5:\"fixed\";b:1;}s:6:\"jsFunc\";a:3:{s:9:\"namespace\";s:25:\"Cronk.grid.ColumnRenderer\";s:8:\"function\";s:13:\"serviceStatus\";s:4:\"type\";s:8:\"renderer\";}}s:6:\"filter\";a:4:{s:7:\"enabled\";b:1;s:4:\"type\";s:5:\"extjs\";s:7:\"subtype\";s:31:\"appkit.ext.filter.servicestatus\";s:11:\"no_operator\";b:0;}s:5:\"order\";a:2:{s:7:\"enabled\";b:1;s:7:\"default\";b:0;}}}}}\";','84ff4e8a17b159a2ba9bbf31cec8493d','2015-06-16 16:50:16','2015-06-17 08:51:20');
/*!40000 ALTER TABLE `nsm_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nsm_target`
--

DROP TABLE IF EXISTS `nsm_target`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nsm_target` (
  `target_id` int(11) NOT NULL AUTO_INCREMENT,
  `target_name` varchar(45) NOT NULL,
  `target_description` varchar(100) DEFAULT NULL,
  `target_class` varchar(80) DEFAULT NULL,
  `target_type` varchar(45) NOT NULL,
  PRIMARY KEY (`target_id`),
  UNIQUE KEY `target_key_unique_target_name_idx` (`target_name`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nsm_target`
--

LOCK TABLES `nsm_target` WRITE;
/*!40000 ALTER TABLE `nsm_target` DISABLE KEYS */;
INSERT INTO `nsm_target` VALUES (1,'IcingaHostgroup','Limit data access to specific hostgroups','IcingaDataHostgroupPrincipalTarget','icinga'),(2,'IcingaServicegroup','Limit data access to specific servicegroups','IcingaDataServicegroupPrincipalTarget','icinga'),(3,'IcingaHostCustomVariablePair','Limit data access to specific custom variables','IcingaDataHostCustomVariablePrincipalTarget','icinga'),(4,'IcingaServiceCustomVariablePair','Limit data access to specific custom variables','IcingaDataServiceCustomVariablePrincipalTarget','icinga'),(5,'IcingaContactgroup','Limit data access to users contact group membership','IcingaDataContactgroupPrincipalTarget','icinga'),(6,'IcingaCommandRo','Limit access to commands','IcingaDataCommandRoPrincipalTarget','icinga'),(7,'appkit.access','Access to login-page (which, actually, means no access)','','credential'),(8,'icinga.user','Access to icinga','','credential'),(9,'appkit.admin.groups','Access to group related data (e.g. share cronks)','','credential'),(10,'appkit.admin.users','Access to user related data (provider)','','credential'),(11,'appkit.admin','Access to admin panel ','','credential'),(12,'appkit.user.dummy','Basic right for users','','credential'),(13,'appkit.api.access','Access to web-based api adapter','','credential'),(14,'icinga.demoMode','Hide features like password reset which are not wanted in demo systems','','credential'),(15,'icinga.cronk.category.admin','Enables category admin features','','credential'),(16,'icinga.cronk.log','Allow user to view icinga-log','','credential'),(17,'icinga.control.view','Allow user to view icinga status','','credential'),(18,'icinga.control.admin','Allow user to administrate the icinga process','','credential'),(19,'IcingaCommandRestrictions','Disable critical commands for this user','IcingaDataCommandRestrictionPrincipalTarget','icinga'),(20,'icinga.cronk.custom','Allow user to create and modify custom cronks','','credential'),(21,'icinga.cronk.admin','Allow user to edit and delete all cronks','','credential'),(22,'IcingaService','Limit data access to specific services','IcingaDataServicePrincipalTarget','icinga'),(23,'IcingaHost','Limit data access to specific hosts','IcingaDataHostPrincipalTarget','icinga');
/*!40000 ALTER TABLE `nsm_target` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nsm_target_value`
--

DROP TABLE IF EXISTS `nsm_target_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nsm_target_value` (
  `tv_pt_id` int(11) NOT NULL DEFAULT '0',
  `tv_key` varchar(45) NOT NULL DEFAULT '',
  `tv_val` varchar(45) NOT NULL,
  PRIMARY KEY (`tv_pt_id`,`tv_key`),
  CONSTRAINT `nsm_target_value_tv_pt_id_nsm_principal_target_pt_id` FOREIGN KEY (`tv_pt_id`) REFERENCES `nsm_principal_target` (`pt_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nsm_target_value`
--

LOCK TABLES `nsm_target_value` WRITE;
/*!40000 ALTER TABLE `nsm_target_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `nsm_target_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nsm_user`
--

DROP TABLE IF EXISTS `nsm_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nsm_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_account` int(11) NOT NULL DEFAULT '0',
  `user_name` varchar(127) NOT NULL,
  `user_lastname` varchar(40) NOT NULL,
  `user_firstname` varchar(40) NOT NULL,
  `user_password` varchar(64) NOT NULL,
  `user_salt` varchar(64) NOT NULL,
  `user_authsrc` varchar(45) NOT NULL DEFAULT 'internal',
  `user_authid` text,
  `user_authkey` varchar(64) DEFAULT NULL,
  `user_email` varchar(254) NOT NULL,
  `user_description` varchar(255) DEFAULT NULL,
  `user_disabled` tinyint(4) NOT NULL DEFAULT '1',
  `user_created` datetime NOT NULL,
  `user_modified` datetime NOT NULL,
  `user_last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name_unique_idx` (`user_name`),
  KEY `user_search_idx` (`user_name`,`user_authsrc`,`user_disabled`,`user_authid`(127))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nsm_user`
--

LOCK TABLES `nsm_user` WRITE;
/*!40000 ALTER TABLE `nsm_user` DISABLE KEYS */;
INSERT INTO `nsm_user` VALUES (1,0,'root','Root','Enoch','9da166fe7a0ab4c2b2c2709b5a61249f7d69c1c566cf59f70b1ab3a4ab7997f5','cddc64ba9346d3ed61535634cf739d8c421aa9b37ac2ab04be2c8658fd6d8649','internal',NULL,NULL,'root@localhost.local',NULL,0,'2015-06-16 18:19:07','2015-06-17 08:51:19','2015-06-17 08:51:19');
/*!40000 ALTER TABLE `nsm_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nsm_user_preference`
--

DROP TABLE IF EXISTS `nsm_user_preference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nsm_user_preference` (
  `upref_id` int(11) NOT NULL AUTO_INCREMENT,
  `upref_user_id` int(11) NOT NULL,
  `upref_val` varchar(100) DEFAULT NULL,
  `upref_longval` longtext,
  `upref_key` varchar(50) NOT NULL,
  `upref_created` datetime NOT NULL,
  `upref_modified` datetime NOT NULL,
  PRIMARY KEY (`upref_id`),
  UNIQUE KEY `upref_user_key_unique_idx` (`upref_user_id`,`upref_key`),
  KEY `upref_search_key_idx_idx` (`upref_key`),
  KEY `principal_role_id_ix_idx` (`upref_user_id`),
  CONSTRAINT `nsm_user_preference_upref_user_id_nsm_user_user_id` FOREIGN KEY (`upref_user_id`) REFERENCES `nsm_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nsm_user_preference`
--

LOCK TABLES `nsm_user_preference` WRITE;
/*!40000 ALTER TABLE `nsm_user_preference` DISABLE KEYS */;
INSERT INTO `nsm_user_preference` VALUES (1,1,NULL,'[{\"name\":\"cronk-listing-panel\",\"value\":\"{\\\"collapsed_categories\\\":[\\\"to\\\",\\\"misc\\\"],\\\"cronkliststyle\\\":\\\"list\\\",\\\"tabslider_active\\\":false}\"},{\"name\":\"cronk-tab-panel\",\"value\":\"{\\\"cronks\\\":{\\\"cr-panel-1434473847\\\":{\\\"params\\\":{\\\"module\\\":\\\"Cronks\\\",\\\"action\\\":\\\"System.PortalHello\\\"},\\\"crname\\\":\\\"portalHello\\\",\\\"autoRefresh\\\":true,\\\"cdata\\\":{},\\\"cenv\\\":{},\\\"autoLayout\\\":false,\\\"cmpid\\\":\\\"cronk-cid1434473849\\\",\\\"stateuid\\\":\\\"cronk-sid1434473848\\\",\\\"parentid\\\":\\\"cr-panel-1434473847\\\",\\\"title\\\":\\\"Welcome\\\",\\\"xtype\\\":\\\"cronk\\\",\\\"closable\\\":true,\\\"border\\\":false,\\\"id\\\":\\\"cr-panel-1434473847\\\",\\\"iconCls\\\":\\\"icinga-cronk-icon-start\\\"},\\\"cr-panel-1434473850\\\":{\\\"params\\\":{\\\"module\\\":\\\"Cronks\\\",\\\"action\\\":\\\"System.ViewProc\\\",\\\"template\\\":\\\"icinga-all-service-problems\\\",\\\"storeDisableAutoload\\\":1},\\\"crname\\\":\\\"gridAllServiceProblemsView\\\",\\\"autoRefresh\\\":true,\\\"cdata\\\":{},\\\"cenv\\\":{},\\\"autoLayout\\\":false,\\\"cmpid\\\":\\\"cronk-cid1434473852\\\",\\\"stateuid\\\":\\\"cronk-sid1434473851\\\",\\\"parentid\\\":\\\"cr-panel-1434473850\\\",\\\"title\\\":\\\"All service problems\\\",\\\"xtype\\\":\\\"cronk\\\",\\\"closable\\\":true,\\\"border\\\":false,\\\"id\\\":\\\"cr-panel-1434473850\\\",\\\"iconCls\\\":\\\"icinga-cronk-icon-tool\\\"},\\\"cr-panel-1434473853\\\":{\\\"params\\\":{\\\"module\\\":\\\"Cronks\\\",\\\"action\\\":\\\"System.ViewProc\\\",\\\"template\\\":\\\"icinga-all-host-problems\\\",\\\"storeDisableAutoload\\\":1},\\\"crname\\\":\\\"gridAllHostProblemsView\\\",\\\"autoRefresh\\\":true,\\\"cdata\\\":{},\\\"cenv\\\":{},\\\"autoLayout\\\":false,\\\"cmpid\\\":\\\"cronk-cid1434473855\\\",\\\"stateuid\\\":\\\"cronk-sid1434473854\\\",\\\"parentid\\\":\\\"cr-panel-1434473853\\\",\\\"title\\\":\\\"All host problems\\\",\\\"xtype\\\":\\\"cronk\\\",\\\"closable\\\":true,\\\"border\\\":false,\\\"id\\\":\\\"cr-panel-1434473853\\\",\\\"iconCls\\\":\\\"icinga-cronk-icon-tool\\\"},\\\"cr-panel-1434473856\\\":{\\\"params\\\":{\\\"module\\\":\\\"Cronks\\\",\\\"action\\\":\\\"System.ViewProc\\\",\\\"template\\\":\\\"icinga-unhandled-host-problems\\\",\\\"storeDisableAutoload\\\":1},\\\"crname\\\":\\\"gridUnhandledHostProblemsView\\\",\\\"autoRefresh\\\":true,\\\"cdata\\\":{},\\\"cenv\\\":{},\\\"autoLayout\\\":false,\\\"cmpid\\\":\\\"cronk-cid1434473858\\\",\\\"stateuid\\\":\\\"cronk-sid1434473857\\\",\\\"parentid\\\":\\\"cr-panel-1434473856\\\",\\\"title\\\":\\\"Unhandled host problems\\\",\\\"xtype\\\":\\\"cronk\\\",\\\"closable\\\":true,\\\"border\\\":false,\\\"id\\\":\\\"cr-panel-1434473856\\\",\\\"iconCls\\\":\\\"icinga-cronk-icon-warning\\\"},\\\"cr-panel-1434531014\\\":{\\\"params\\\":{\\\"module\\\":\\\"Cronks\\\",\\\"action\\\":\\\"System.ViewProc\\\",\\\"template\\\":\\\"icinga-all-service-problems\\\",\\\"storeDisableAutoload\\\":1},\\\"crname\\\":\\\"gridAllServiceProblemsView\\\",\\\"autoRefresh\\\":true,\\\"cdata\\\":{},\\\"cenv\\\":{},\\\"autoLayout\\\":false,\\\"cmpid\\\":\\\"cronk-cid1434531016\\\",\\\"stateuid\\\":\\\"cronk-sid1434531015\\\",\\\"parentid\\\":\\\"cr-panel-1434531014\\\",\\\"title\\\":\\\"All service problems\\\",\\\"xtype\\\":\\\"cronk\\\",\\\"closable\\\":true,\\\"border\\\":false,\\\"id\\\":\\\"cr-panel-1434531014\\\",\\\"iconCls\\\":\\\"icinga-cronk-icon-tool\\\"},\\\"cr-panel-1434531017\\\":{\\\"params\\\":{\\\"module\\\":\\\"Cronks\\\",\\\"action\\\":\\\"System.ViewProc\\\",\\\"template\\\":\\\"icinga-open-problems-template\\\",\\\"storeDisableAutoload\\\":1},\\\"crname\\\":\\\"gridOpenProblemsView\\\",\\\"autoRefresh\\\":true,\\\"cdata\\\":{},\\\"cenv\\\":{},\\\"autoLayout\\\":false,\\\"cmpid\\\":\\\"cronk-cid1434531019\\\",\\\"stateuid\\\":\\\"cronk-sid1434531018\\\",\\\"parentid\\\":\\\"cr-panel-1434531017\\\",\\\"title\\\":\\\"Open problems\\\",\\\"xtype\\\":\\\"cronk\\\",\\\"closable\\\":true,\\\"border\\\":false,\\\"id\\\":\\\"cr-panel-1434531017\\\",\\\"iconCls\\\":\\\"icinga-cronk-icon-warning\\\"},\\\"cr-panel-1434531020\\\":{\\\"params\\\":{\\\"module\\\":\\\"Cronks\\\",\\\"action\\\":\\\"System.ViewProc\\\",\\\"template\\\":\\\"icinga-unhandled-service-problems\\\",\\\"storeDisableAutoload\\\":1},\\\"crname\\\":\\\"gridUnhandledServiceProblemsView\\\",\\\"autoRefresh\\\":true,\\\"cdata\\\":{},\\\"cenv\\\":{},\\\"autoLayout\\\":false,\\\"cmpid\\\":\\\"cronk-cid1434531022\\\",\\\"stateuid\\\":\\\"cronk-sid1434531021\\\",\\\"parentid\\\":\\\"cr-panel-1434531020\\\",\\\"title\\\":\\\"Unhandled service problems\\\",\\\"xtype\\\":\\\"cronk\\\",\\\"closable\\\":true,\\\"border\\\":false,\\\"id\\\":\\\"cr-panel-1434531020\\\",\\\"statefulObjectId\\\":\\\"ext-comp-1136\\\",\\\"iconCls\\\":\\\"icinga-cronk-icon-warning\\\"}},\\\"items\\\":7,\\\"active\\\":\\\"cr-panel-1434531020\\\",\\\"tabOrder\\\":[\\\"cr-panel-1434473847\\\",\\\"cr-panel-1434473850\\\",\\\"cr-panel-1434473853\\\",\\\"cr-panel-1434473856\\\",\\\"cr-panel-1434531014\\\",\\\"cr-panel-1434531017\\\",\\\"cr-panel-1434531020\\\"]}\"},{\"name\":\"cronk-sid1434473851\",\"value\":\"{\\\"filter_params\\\":{},\\\"filter_types\\\":{},\\\"nativeState\\\":{\\\"columns\\\":[{\\\"id\\\":\\\"checker\\\",\\\"width\\\":20},{\\\"id\\\":\\\"event-sub-frame\\\",\\\"width\\\":20},{\\\"id\\\":0,\\\"width\\\":25},{\\\"id\\\":1,\\\"width\\\":60,\\\"hidden\\\":true},{\\\"id\\\":2,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":3,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":4,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":5,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":6,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":7,\\\"width\\\":180},{\\\"id\\\":8,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":9,\\\"width\\\":22},{\\\"id\\\":10,\\\"width\\\":60},{\\\"id\\\":11,\\\"width\\\":100},{\\\"id\\\":12,\\\"width\\\":120},{\\\"id\\\":13,\\\"width\\\":110},{\\\"id\\\":14,\\\"width\\\":50},{\\\"id\\\":15,\\\"width\\\":250},{\\\"id\\\":16,\\\"width\\\":60},{\\\"id\\\":17,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":18,\\\"width\\\":25}],\\\"sort\\\":{\\\"field\\\":\\\"host_name\\\",\\\"direction\\\":\\\"ASC\\\"},\\\"group\\\":\\\"host_name\\\"},\\\"store_origin_params\\\":{},\\\"sortToggle\\\":{\\\"host_name\\\":\\\"ASC\\\"},\\\"sortInfo\\\":{\\\"field\\\":\\\"host_name\\\",\\\"direction\\\":\\\"ASC\\\"},\\\"colModel\\\":{\\\"groupField\\\":\\\"host_name\\\",\\\"columns\\\":[{\\\"hidden\\\":false,\\\"width\\\":25,\\\"dataIndex\\\":\\\"service_icon\\\",\\\"id\\\":0,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":60,\\\"dataIndex\\\":\\\"instance_name\\\",\\\"id\\\":1,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_object_id\\\",\\\"id\\\":2,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_object_id\\\",\\\"id\\\":3,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_name\\\",\\\"id\\\":4,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_alias\\\",\\\"id\\\":5,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_display_name\\\",\\\"id\\\":6,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":180,\\\"dataIndex\\\":\\\"service_name\\\",\\\"id\\\":7,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_display_name\\\",\\\"id\\\":8,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":22,\\\"dataIndex\\\":\\\"comments\\\",\\\"id\\\":9,\\\"sortable\\\":false},{\\\"hidden\\\":false,\\\"width\\\":60,\\\"dataIndex\\\":\\\"service_state_type\\\",\\\"id\\\":10,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_status\\\",\\\"id\\\":11,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":120,\\\"dataIndex\\\":\\\"service_last_check\\\",\\\"id\\\":12,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":110,\\\"dataIndex\\\":\\\"service_duration\\\",\\\"id\\\":13,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":50,\\\"dataIndex\\\":\\\"service_info_icons\\\",\\\"id\\\":14,\\\"sortable\\\":false},{\\\"hidden\\\":false,\\\"width\\\":250,\\\"dataIndex\\\":\\\"plugin_output\\\",\\\"id\\\":15,\\\"sortable\\\":false},{\\\"hidden\\\":false,\\\"width\\\":60,\\\"dataIndex\\\":\\\"service_check_attempt\\\",\\\"id\\\":16,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_hard_status\\\",\\\"id\\\":17,\\\"sortable\\\":true},{\\\"width\\\":25,\\\"dataIndex\\\":\\\"__\\\",\\\"id\\\":18,\\\"sortable\\\":false},{\\\"dataIndex\\\":\\\"id\\\"},{\\\"width\\\":20,\\\"dataIndex\\\":\\\"id\\\",\\\"id\\\":\\\"event-sub-frame\\\",\\\"sortable\\\":false}],\\\"groupDir\\\":\\\"ASC\\\",\\\"groupOnSort\\\":true},\\\"autoRefresh\\\":1}\"},{\"name\":\"cronk-sid1434473854\",\"value\":\"{\\\"filter_params\\\":{},\\\"filter_types\\\":{},\\\"nativeState\\\":{\\\"columns\\\":[{\\\"id\\\":\\\"checker\\\",\\\"width\\\":20},{\\\"id\\\":\\\"event-sub-frame\\\",\\\"width\\\":20},{\\\"id\\\":0,\\\"width\\\":25},{\\\"id\\\":1,\\\"width\\\":60,\\\"hidden\\\":true},{\\\"id\\\":2,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":3,\\\"width\\\":150},{\\\"id\\\":4,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":5,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":6,\\\"width\\\":22},{\\\"id\\\":7,\\\"width\\\":60},{\\\"id\\\":8,\\\"width\\\":100},{\\\"id\\\":9,\\\"width\\\":120},{\\\"id\\\":10,\\\"width\\\":110},{\\\"id\\\":11,\\\"width\\\":50},{\\\"id\\\":12,\\\"width\\\":250},{\\\"id\\\":13,\\\"width\\\":60},{\\\"id\\\":14,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":15,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":16,\\\"width\\\":25}],\\\"sort\\\":{\\\"field\\\":\\\"host_object_id\\\",\\\"direction\\\":\\\"ASC\\\"}},\\\"store_origin_params\\\":{},\\\"sortToggle\\\":{\\\"host_object_id\\\":\\\"ASC\\\"},\\\"sortInfo\\\":{\\\"field\\\":\\\"host_object_id\\\",\\\"direction\\\":\\\"ASC\\\"},\\\"colModel\\\":{\\\"groupField\\\":null,\\\"columns\\\":[{\\\"hidden\\\":false,\\\"width\\\":25,\\\"dataIndex\\\":\\\"host_icon\\\",\\\"id\\\":0,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":60,\\\"dataIndex\\\":\\\"instance_name\\\",\\\"id\\\":1,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_object_id\\\",\\\"id\\\":2,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":150,\\\"dataIndex\\\":\\\"host_name\\\",\\\"id\\\":3,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_alias\\\",\\\"id\\\":4,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_display_name\\\",\\\"id\\\":5,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":22,\\\"dataIndex\\\":\\\"comments\\\",\\\"id\\\":6,\\\"sortable\\\":false},{\\\"hidden\\\":false,\\\"width\\\":60,\\\"dataIndex\\\":\\\"host_state_type\\\",\\\"id\\\":7,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_status\\\",\\\"id\\\":8,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":120,\\\"dataIndex\\\":\\\"host_last_check\\\",\\\"id\\\":9,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":110,\\\"dataIndex\\\":\\\"host_duration\\\",\\\"id\\\":10,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":50,\\\"dataIndex\\\":\\\"host_info_icons\\\",\\\"id\\\":11,\\\"sortable\\\":false},{\\\"hidden\\\":false,\\\"width\\\":250,\\\"dataIndex\\\":\\\"plugin_output\\\",\\\"id\\\":12,\\\"sortable\\\":false},{\\\"hidden\\\":false,\\\"width\\\":60,\\\"dataIndex\\\":\\\"check_attempt\\\",\\\"id\\\":13,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"max_attempt\\\",\\\"id\\\":14,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_hard_status\\\",\\\"id\\\":15,\\\"sortable\\\":true},{\\\"width\\\":25,\\\"dataIndex\\\":\\\"__\\\",\\\"id\\\":16,\\\"sortable\\\":false},{\\\"dataIndex\\\":\\\"id\\\"},{\\\"width\\\":20,\\\"dataIndex\\\":\\\"id\\\",\\\"id\\\":\\\"event-sub-frame\\\",\\\"sortable\\\":false}]},\\\"autoRefresh\\\":1}\"},{\"name\":\"cronk-sid1434473857\",\"value\":\"{\\\"filter_params\\\":{},\\\"filter_types\\\":{},\\\"nativeState\\\":{\\\"columns\\\":[{\\\"id\\\":\\\"checker\\\",\\\"width\\\":20},{\\\"id\\\":\\\"event-sub-frame\\\",\\\"width\\\":20},{\\\"id\\\":0,\\\"width\\\":25},{\\\"id\\\":1,\\\"width\\\":60,\\\"hidden\\\":true},{\\\"id\\\":2,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":3,\\\"width\\\":150},{\\\"id\\\":4,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":5,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":6,\\\"width\\\":22},{\\\"id\\\":7,\\\"width\\\":60},{\\\"id\\\":8,\\\"width\\\":100},{\\\"id\\\":9,\\\"width\\\":120},{\\\"id\\\":10,\\\"width\\\":110},{\\\"id\\\":11,\\\"width\\\":50},{\\\"id\\\":12,\\\"width\\\":250},{\\\"id\\\":13,\\\"width\\\":60},{\\\"id\\\":14,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":15,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":16,\\\"width\\\":25}],\\\"sort\\\":{\\\"field\\\":\\\"host_object_id\\\",\\\"direction\\\":\\\"ASC\\\"}},\\\"store_origin_params\\\":{},\\\"sortToggle\\\":{\\\"host_object_id\\\":\\\"ASC\\\"},\\\"sortInfo\\\":{\\\"field\\\":\\\"host_object_id\\\",\\\"direction\\\":\\\"ASC\\\"},\\\"colModel\\\":{\\\"groupField\\\":null,\\\"columns\\\":[{\\\"width\\\":25,\\\"dataIndex\\\":\\\"host_icon\\\",\\\"id\\\":0,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":60,\\\"dataIndex\\\":\\\"instance_name\\\",\\\"id\\\":1,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_object_id\\\",\\\"id\\\":2,\\\"sortable\\\":true},{\\\"width\\\":150,\\\"dataIndex\\\":\\\"host_name\\\",\\\"id\\\":3,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_alias\\\",\\\"id\\\":4,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_display_name\\\",\\\"id\\\":5,\\\"sortable\\\":true},{\\\"width\\\":22,\\\"dataIndex\\\":\\\"comments\\\",\\\"id\\\":6,\\\"sortable\\\":false},{\\\"width\\\":60,\\\"dataIndex\\\":\\\"host_state_type\\\",\\\"id\\\":7,\\\"sortable\\\":true},{\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_status\\\",\\\"id\\\":8,\\\"sortable\\\":true},{\\\"width\\\":120,\\\"dataIndex\\\":\\\"host_last_check\\\",\\\"id\\\":9,\\\"sortable\\\":true},{\\\"width\\\":110,\\\"dataIndex\\\":\\\"host_duration\\\",\\\"id\\\":10,\\\"sortable\\\":true},{\\\"width\\\":50,\\\"dataIndex\\\":\\\"host_info_icons\\\",\\\"id\\\":11,\\\"sortable\\\":false},{\\\"width\\\":250,\\\"dataIndex\\\":\\\"plugin_output\\\",\\\"id\\\":12,\\\"sortable\\\":false},{\\\"width\\\":60,\\\"dataIndex\\\":\\\"check_attempt\\\",\\\"id\\\":13,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"max_attempt\\\",\\\"id\\\":14,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_hard_status\\\",\\\"id\\\":15,\\\"sortable\\\":true},{\\\"width\\\":25,\\\"dataIndex\\\":\\\"__\\\",\\\"id\\\":16,\\\"sortable\\\":false},{\\\"dataIndex\\\":\\\"id\\\"},{\\\"width\\\":20,\\\"dataIndex\\\":\\\"id\\\",\\\"id\\\":\\\"event-sub-frame\\\",\\\"sortable\\\":false}]},\\\"autoRefresh\\\":1}\"},{\"name\":\"view-container\",\"value\":\"null\"},{\"name\":\"west-frame\",\"value\":\"{}\"},{\"name\":\"cronk-sid1434531015\",\"value\":\"{\\\"filter_params\\\":{},\\\"filter_types\\\":{},\\\"nativeState\\\":{\\\"columns\\\":[{\\\"id\\\":\\\"checker\\\",\\\"width\\\":20},{\\\"id\\\":\\\"event-sub-frame\\\",\\\"width\\\":20},{\\\"id\\\":0,\\\"width\\\":25},{\\\"id\\\":1,\\\"width\\\":60,\\\"hidden\\\":true},{\\\"id\\\":2,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":3,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":4,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":5,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":6,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":7,\\\"width\\\":180},{\\\"id\\\":8,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":9,\\\"width\\\":22},{\\\"id\\\":10,\\\"width\\\":60},{\\\"id\\\":11,\\\"width\\\":100},{\\\"id\\\":12,\\\"width\\\":120},{\\\"id\\\":13,\\\"width\\\":110},{\\\"id\\\":14,\\\"width\\\":50},{\\\"id\\\":15,\\\"width\\\":250},{\\\"id\\\":16,\\\"width\\\":60},{\\\"id\\\":17,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":18,\\\"width\\\":25}],\\\"sort\\\":{\\\"field\\\":\\\"host_name\\\",\\\"direction\\\":\\\"ASC\\\"},\\\"group\\\":\\\"host_name\\\"},\\\"store_origin_params\\\":{},\\\"sortToggle\\\":{\\\"host_name\\\":\\\"ASC\\\"},\\\"sortInfo\\\":{\\\"field\\\":\\\"host_name\\\",\\\"direction\\\":\\\"ASC\\\"},\\\"colModel\\\":{\\\"groupField\\\":\\\"host_name\\\",\\\"columns\\\":[{\\\"hidden\\\":false,\\\"width\\\":25,\\\"dataIndex\\\":\\\"service_icon\\\",\\\"id\\\":0,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":60,\\\"dataIndex\\\":\\\"instance_name\\\",\\\"id\\\":1,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_object_id\\\",\\\"id\\\":2,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_object_id\\\",\\\"id\\\":3,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_name\\\",\\\"id\\\":4,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_alias\\\",\\\"id\\\":5,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_display_name\\\",\\\"id\\\":6,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":180,\\\"dataIndex\\\":\\\"service_name\\\",\\\"id\\\":7,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_display_name\\\",\\\"id\\\":8,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":22,\\\"dataIndex\\\":\\\"comments\\\",\\\"id\\\":9,\\\"sortable\\\":false},{\\\"hidden\\\":false,\\\"width\\\":60,\\\"dataIndex\\\":\\\"service_state_type\\\",\\\"id\\\":10,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_status\\\",\\\"id\\\":11,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":120,\\\"dataIndex\\\":\\\"service_last_check\\\",\\\"id\\\":12,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":110,\\\"dataIndex\\\":\\\"service_duration\\\",\\\"id\\\":13,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":50,\\\"dataIndex\\\":\\\"service_info_icons\\\",\\\"id\\\":14,\\\"sortable\\\":false},{\\\"hidden\\\":false,\\\"width\\\":250,\\\"dataIndex\\\":\\\"plugin_output\\\",\\\"id\\\":15,\\\"sortable\\\":false},{\\\"hidden\\\":false,\\\"width\\\":60,\\\"dataIndex\\\":\\\"service_check_attempt\\\",\\\"id\\\":16,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_hard_status\\\",\\\"id\\\":17,\\\"sortable\\\":true},{\\\"width\\\":25,\\\"dataIndex\\\":\\\"__\\\",\\\"id\\\":18,\\\"sortable\\\":false},{\\\"dataIndex\\\":\\\"id\\\"},{\\\"width\\\":20,\\\"dataIndex\\\":\\\"id\\\",\\\"id\\\":\\\"event-sub-frame\\\",\\\"sortable\\\":false}],\\\"groupDir\\\":\\\"ASC\\\",\\\"groupOnSort\\\":true},\\\"autoRefresh\\\":1}\"},{\"name\":\"cronk-sid1434531018\",\"value\":\"{\\\"filter_params\\\":{},\\\"filter_types\\\":{},\\\"nativeState\\\":{\\\"columns\\\":[{\\\"id\\\":\\\"checker\\\",\\\"width\\\":20},{\\\"id\\\":0,\\\"width\\\":25},{\\\"id\\\":1,\\\"width\\\":60,\\\"hidden\\\":true},{\\\"id\\\":2,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":3,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":4,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":5,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":6,\\\"width\\\":110},{\\\"id\\\":7,\\\"width\\\":150},{\\\"id\\\":8,\\\"width\\\":60},{\\\"id\\\":9,\\\"width\\\":100},{\\\"id\\\":10,\\\"width\\\":100},{\\\"id\\\":11,\\\"width\\\":200},{\\\"id\\\":12,\\\"width\\\":110},{\\\"id\\\":13,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":14,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":15,\\\"width\\\":125},{\\\"id\\\":16,\\\"width\\\":25}],\\\"sort\\\":{\\\"field\\\":\\\"last_state_change\\\",\\\"direction\\\":\\\"DESC\\\"}},\\\"store_origin_params\\\":{},\\\"sortToggle\\\":{\\\"last_state_change\\\":\\\"DESC\\\"},\\\"sortInfo\\\":{\\\"field\\\":\\\"last_state_change\\\",\\\"direction\\\":\\\"DESC\\\"},\\\"colModel\\\":{\\\"groupField\\\":null,\\\"columns\\\":[{\\\"hidden\\\":false,\\\"width\\\":25,\\\"dataIndex\\\":\\\"icon\\\",\\\"id\\\":0,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":60,\\\"dataIndex\\\":\\\"instance_name\\\",\\\"id\\\":1,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"object_id\\\",\\\"id\\\":2,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"objecttype_id\\\",\\\"id\\\":3,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_object_id\\\",\\\"id\\\":4,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_object_id\\\",\\\"id\\\":5,\\\"sortable\\\":false},{\\\"hidden\\\":false,\\\"width\\\":110,\\\"dataIndex\\\":\\\"host_name\\\",\\\"id\\\":6,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":150,\\\"dataIndex\\\":\\\"service_name\\\",\\\"id\\\":7,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":60,\\\"dataIndex\\\":\\\"state_type\\\",\\\"id\\\":8,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_status\\\",\\\"id\\\":9,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_state\\\",\\\"id\\\":10,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":200,\\\"dataIndex\\\":\\\"output\\\",\\\"id\\\":11,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":110,\\\"dataIndex\\\":\\\"last_state_change\\\",\\\"id\\\":12,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_last_check\\\",\\\"id\\\":13,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_last_check\\\",\\\"id\\\":14,\\\"sortable\\\":true},{\\\"hidden\\\":false,\\\"width\\\":125,\\\"dataIndex\\\":\\\"duration\\\",\\\"id\\\":15,\\\"sortable\\\":true},{\\\"width\\\":25,\\\"dataIndex\\\":\\\"__\\\",\\\"id\\\":16,\\\"sortable\\\":false}]},\\\"autoRefresh\\\":1}\"},{\"name\":\"cronk-sid1434531021\",\"value\":\"{\\\"filter_params\\\":{},\\\"filter_types\\\":{},\\\"nativeState\\\":{\\\"columns\\\":[{\\\"id\\\":\\\"checker\\\",\\\"width\\\":20},{\\\"id\\\":\\\"event-sub-frame\\\",\\\"width\\\":20},{\\\"id\\\":0,\\\"width\\\":25},{\\\"id\\\":1,\\\"width\\\":60,\\\"hidden\\\":true},{\\\"id\\\":2,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":3,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":4,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":5,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":6,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":7,\\\"width\\\":180},{\\\"id\\\":8,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":9,\\\"width\\\":22},{\\\"id\\\":10,\\\"width\\\":60},{\\\"id\\\":11,\\\"width\\\":100},{\\\"id\\\":12,\\\"width\\\":120},{\\\"id\\\":13,\\\"width\\\":110},{\\\"id\\\":14,\\\"width\\\":50},{\\\"id\\\":15,\\\"width\\\":250},{\\\"id\\\":16,\\\"width\\\":60},{\\\"id\\\":17,\\\"width\\\":100,\\\"hidden\\\":true},{\\\"id\\\":18,\\\"width\\\":25}],\\\"sort\\\":{\\\"field\\\":\\\"host_name\\\",\\\"direction\\\":\\\"ASC\\\"},\\\"group\\\":\\\"host_name\\\"},\\\"store_origin_params\\\":{},\\\"sortToggle\\\":{\\\"host_name\\\":\\\"ASC\\\"},\\\"sortInfo\\\":{\\\"field\\\":\\\"host_name\\\",\\\"direction\\\":\\\"ASC\\\"},\\\"colModel\\\":{\\\"groupField\\\":\\\"host_name\\\",\\\"columns\\\":[{\\\"width\\\":25,\\\"dataIndex\\\":\\\"service_icon\\\",\\\"id\\\":0,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":60,\\\"dataIndex\\\":\\\"instance_name\\\",\\\"id\\\":1,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_object_id\\\",\\\"id\\\":2,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_object_id\\\",\\\"id\\\":3,\\\"sortable\\\":false},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_name\\\",\\\"id\\\":4,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_alias\\\",\\\"id\\\":5,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"host_display_name\\\",\\\"id\\\":6,\\\"sortable\\\":true},{\\\"width\\\":180,\\\"dataIndex\\\":\\\"service_name\\\",\\\"id\\\":7,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_display_name\\\",\\\"id\\\":8,\\\"sortable\\\":true},{\\\"width\\\":22,\\\"dataIndex\\\":\\\"comments\\\",\\\"id\\\":9,\\\"sortable\\\":false},{\\\"width\\\":60,\\\"dataIndex\\\":\\\"service_state_type\\\",\\\"id\\\":10,\\\"sortable\\\":true},{\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_status\\\",\\\"id\\\":11,\\\"sortable\\\":true},{\\\"width\\\":120,\\\"dataIndex\\\":\\\"service_last_check\\\",\\\"id\\\":12,\\\"sortable\\\":true},{\\\"width\\\":110,\\\"dataIndex\\\":\\\"service_duration\\\",\\\"id\\\":13,\\\"sortable\\\":true},{\\\"width\\\":50,\\\"dataIndex\\\":\\\"service_info_icons\\\",\\\"id\\\":14,\\\"sortable\\\":false},{\\\"width\\\":250,\\\"dataIndex\\\":\\\"plugin_output\\\",\\\"id\\\":15,\\\"sortable\\\":false},{\\\"width\\\":60,\\\"dataIndex\\\":\\\"service_check_attempt\\\",\\\"id\\\":16,\\\"sortable\\\":true},{\\\"hidden\\\":true,\\\"width\\\":100,\\\"dataIndex\\\":\\\"service_hard_status\\\",\\\"id\\\":17,\\\"sortable\\\":true},{\\\"width\\\":25,\\\"dataIndex\\\":\\\"__\\\",\\\"id\\\":18,\\\"sortable\\\":false},{\\\"dataIndex\\\":\\\"id\\\"},{\\\"width\\\":20,\\\"dataIndex\\\":\\\"id\\\",\\\"id\\\":\\\"event-sub-frame\\\",\\\"sortable\\\":false}],\\\"groupDir\\\":\\\"ASC\\\",\\\"groupOnSort\\\":true},\\\"autoRefresh\\\":1}\"}]','org.icinga.ext.appstate','2015-06-16 16:57:20','2015-06-16 16:57:20');
/*!40000 ALTER TABLE `nsm_user_preference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nsm_user_role`
--

DROP TABLE IF EXISTS `nsm_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nsm_user_role` (
  `usro_user_id` int(11) NOT NULL DEFAULT '0',
  `usro_role_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usro_user_id`,`usro_role_id`),
  KEY `nsm_user_role_ix_idx` (`usro_role_id`),
  CONSTRAINT `nsm_user_role_usro_role_id_nsm_role_role_id` FOREIGN KEY (`usro_role_id`) REFERENCES `nsm_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nsm_user_role_usro_user_id_nsm_user_user_id` FOREIGN KEY (`usro_user_id`) REFERENCES `nsm_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nsm_user_role`
--

LOCK TABLES `nsm_user_role` WRITE;
/*!40000 ALTER TABLE `nsm_user_role` DISABLE KEYS */;
INSERT INTO `nsm_user_role` VALUES (1,1),(1,2),(1,3);
/*!40000 ALTER TABLE `nsm_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-17 10:55:46
