#�/bin/bash
STATE_OK=0;
STATE_WARNING=1;
STATE_CRITICAL=2;

VAR=$(expect /home/runner/test3.exp)
echo -e "$VAR" > /tmp/ace_load.log
sed -e 's///g' /tmp/ace_load.log > /tmp/ace_load2.log

while read LINE 
do  
  if [[ $LINE == *ssl-connections* && $LINE == *rate* ]]
  then
    CX_SSL_PER_SEC_CURRENT=`echo "$LINE" | sed -s 's/ssl-connections rate\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)/\1/'`
    CX_SSL_PER_SEC_PEAK=`echo "$LINE" | sed -s 's/ssl-connections rate\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)/\2/'`
    CX_SSL_PER_SEC_MAX=`echo "$LINE" | sed -s 's/ssl-connections rate\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)/\4/'`
  fi
done < /tmp/ace_load2.log

#echo $CX_SSL_PER_SEC_CURRENT
#echo $CX_SSL_PER_SEC_PEAK
#echo $CX_SSL_PER_SEC_MAX

rm /tmp/ace_load.log
rm /tmp/ace_load2.log

#RES=$(($CX_SSL_PER_SEC_CURRENT * 100 / $CX_SSL_PER_SEC_MAX))
RES=`expr $CX_SSL_PER_SEC_CURRENT \* 100 / $CX_SSL_PER_SEC_MAX`

if test $RES -gt 70
then
        echo "Max HTTPS Connections more than 70% available ($CX_SSL_PER_SEC_CURRENT cx is $RES%)"
        exit $STATE_WARNING
elif test $RES -gt 90
then
	echo "Max HTTPS Connections more than 90% available ($CX_SSL_PER_SEC_CURRENT cx is $RES%)"
	exit $STATE_CRITICAL
else
        echo "Max HTTPS Connections used ok ($CX_SSL_PER_SEC_CURRENT cx is $RES%)"
        exit $STATE_OK
fi
