#!/bin/bash
#############################################################################################################################################
# Author: EDEL Laurent                                                                                                                      #
# Email : laurent@fifty-five.com                                                                                                            #
#############################################################################################################################################

if test $# -eq 0
then
  echo "NEED DNS AND HOSTNAME ARGUMENT ! bnb BNB for instance "
  exit 3
fi
date > /tmp/DASHBOARD_LOGIN_CHECK

DNS=$1
#HOSTNAME2=`echo $1 | tr '[:lower:]' '[:upper:]'`
HOSTNAME=$2
#EXPECTED="https://$DNS.55labs.com/$HOSTNAME/index.html?locale="
EXPECTED="https://$DNS.55labs.com/$HOSTNAME/index.html"
#EXPECTED_FAILOVER="https://$DNS.55labs.com/$HOSTNAME_1/index.html?locale="
EXPECTED_FAILOVER="https://$DNS.55labs.com/${HOSTNAME}_1/"
INVALID_USER="https://$DNS.55labs.com/$HOSTNAME/login.html?message=Invalid%2Buser%2Bor%2Bpassword"

LOGIN=$3
PASSWORD=$4

if test -z $LOGIN
then
  LOGIN="nagios@fifty-five.com"
fi

if test -z $PASSWORD
then
  PASSWORD="nagios143066!"
fi

rm -f /tmp/header$DNS
touch /tmp/header$DNS

echo curl  -d "login=$LOGIN" -d "password=$PASSWORD" -D /tmp/header$DNS -s https://$DNS.55labs.com/$HOSTNAME/flogin 
LOC=`curl  -d "login=$LOGIN" -d "password=$PASSWORD" -D /tmp/header$DNS -s https://$DNS.55labs.com/$HOSTNAME/flogin > /dev/null`
LOCATION=`cat /tmp/header$DNS | grep Location: | sed "s/Location: //"`

# on vire le \r a la fin qui nous empeche de faire l'egalite sinon !
LOCATION=$(echo $LOCATION|sed 's/\r//g')
echo "LOCATION : $LOCATION"
echo "EXPECTED : ${EXPECTED}"
echo "EXPECTED : ${INVALID_USER}"

if test -z $LOCATION
then
  echo "LOCATION variable empty in /tmp/header$DNS, unknow state"
  exit 3
elif [[ $LOCATION == ${EXPECTED}* || $LOCATION == ${EXPECTED_FAILOVER}* ]]
then
  echo "$DNS OK"
  exit 0
elif [[ $LOCATION == ${INVALID_USER}* ]]
then
  echo "$DNS OK but LOGIN FAILED"
  exit 1
else
  echo "$DNS KO : \"$LOCATION\" vs $EXPECTED or $EXPECTED_FAILOVER"
  exit 1
fi


