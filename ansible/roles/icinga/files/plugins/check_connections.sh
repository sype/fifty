#�/bin/bash
STATE_OK=0;
STATE_WARNING=1;
STATE_CRITICAL=2;

VAR=$(expect /home/runner/test3.exp)
echo -e "$VAR" > /tmp/ace_load.log
sed -e 's///g' /tmp/ace_load.log > /tmp/ace_load2.log

while read LINE 
do  
  if [[ $LINE == *conc-connections* ]]
  then
    CONC_CONNECTIONS_CURRENT=`echo "$LINE" | sed -s 's/conc-connections\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)/\1/'`
    CONC_CONNECTIONS_PEAK=`echo "$LINE" | sed -s 's/conc-connections\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)/\2/'`
    CONC_CONNECTIONS_MAX=`echo "$LINE" | sed -s 's/conc-connections\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)/\4/'`
  fi
done < /tmp/ace_load2.log

#echo $CONC_CONNECTIONS_CURRENT
#echo $CONC_CONNECTIONS_PEAK
#echo $CONC_CONNECTIONS_MAX

rm /tmp/ace_load.log
rm /tmp/ace_load2.log

#RES=$(($CONC_CONNECTIONS_CURRENT * 100 / $CONC_CONNECTIONS_MAX))
RES=`expr $CONC_CONNECTIONS_CURRENT \* 100 / $CONC_CONNECTIONS_MAX`

if test $RES -gt 70
then
        echo "Established connections more than 70% available ($CONC_CONNECTIONS_CURRENT cx is $RES%)"
        exit $STATE_WARNING
elif test $RES -gt 90
then
	echo "Established connections more than 90% available ($CONC_CONNECTIONS_CURRENT cx is $RES%)"
	exit $STATE_CRITICAL
else
        echo "Established connections used ok ($CONC_CONNECTIONS_CURRENT cx is $RES%)"
        exit $STATE_OK
fi
