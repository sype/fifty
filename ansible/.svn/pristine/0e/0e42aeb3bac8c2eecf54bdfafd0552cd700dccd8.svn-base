---

- name: bootstrap - install ansible dependencies
  apt:  name={{ item }} update_cache=yes state=latest  
  with_items:
  - python-apt
  - resolvconf
  - python-pycurl
  - aptitude    
  - build-essential
  - autoconf
  - python-pip
  - python-passlib
  - libmagic1
  -  ntp
  - sysstat
  - unzip 
  - snmpd 
  - ufw 
  - htop
  - apache2
  - sendmail
  - backup-manager
  - ant
  - cronolog
  - build-essential
  - subversion 
  tags:
  - packages 
  sudo_user: root



- name:  "configure the ntp"
  file: path=ntp.conf   dest=/etc/ntp.conf 
  sudo_user: root
  

- name: "restart the ntp service" 
  service: name=ntp state=restarted 
  sudo_user: root

- name :  "remove IPV6 in sysctl.d"
  template: src="60-remove-ipv6.conf.j2"
              dest="/etc/sysctl.d/60-remove-ipv6.conf"
              owner=root group=root mode=544 force=yes
  sudo_user: root
  tags:
  - kernel

- name: "add the localhost to /etc/hosts"
  lineinfile: dest=/etc/hosts regexp='^127\.0\.0\.1' line='127.0.0.1 localhost'
  sudo_user: root
  tags: hostname

- name: "add the host name to /etc/hosts"
  lineinfile: dest=/etc/hosts regexp='^{{ hostvars[inventory_hostname]["ansible_default_ipv4"]["address"] }}' line='{{ hostvars[inventory_hostname]["ansible_default_ipv4"]["address"] }} {{ inventory_hostname }}.55labs.com {{ inventory_hostname }}'
  sudo_user: root
  tags: hostname

- name: "configure hostname"
#  lineinfile: dest=/etc/hostname line='{{ inventory_hostname }}.55labs.com'
  hostname: name={{ inventory_hostname }}.55labs.com
  sudo_user: root
  tags:
   - common




- name: send dns resolver config
  file: path=dns.txt dest=/etc/resolvconf/resolv.conf.d/base
  sudo_user: root
  tags:
   - dns

- name: reload resol.conf
  shell: resolvconf -u
  sudo_user: root
  tags: 
   - dns 

- name: add user
  user: name={{ item }} groups=root,syslog,adm home=/home/{{ item }} state=present
  with_items:
   - installer
   - "{{ admin_user }}" 
  sudo_user: root


- name: add user into group
  shell: usermod -g root {{ item }}
  with_items:
  - installer
  - "{{ admin_user }}" 


- name : "add user  runner and installer to sudoers"
  copy : content="{{ item }} ALL=(ALL) NOPASSWD:ALL" dest="/etc/sudoers.d/{{ item }}"
            owner=root group=root mode=440 force=yes
  with_items:
   - installer
   - "{{ admin_user }}" 
  sudo_user: root
  tags: users



- name: "setup google DNS "
  lineinfile: dest="/etc/network/interfaces"
                regexp="dns-nameservers"
                line="dns-nameservers 213.186.33.99 8.8.8.8"
  sudo_user: root
  tags: dns


 

- name: set the locale
  template: src=templates/bash_profile  dest=/home/installer/.bashrc  
  sudo_user: root


- name: set ulimit configuration
  file: path=limits.conf       dest=/etc/security/limits.conf 
  sudo_user: root


- name: install gcutil
  get_url: url=https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.zip dest=/tmp
  sudo: yes
  tags:
   - cge_tools


- name: unzip gcutil
  unarchive: src=/tmp/google-cloud-sdk.zip  dest=/home/{{ admin_user }}/ copy=no 
  sudo: yes
  tags:
   - gce_tools



- name: copy the netrc file for backup
  copy: src=files/netrc         dest=/home/{{ admin_user }}/.netrc
  tags:
   - commons


- name: copy the bashrc_alias
  copy: src=files/bashrc_aliases     dest=/home/{{ admin_user }}/.bashrc_aliases
  tags:
   - commons




#- name: set the ssh port
#  template: path=templates/ssh.j2   dest=/etc/ssh/ssh_config
#  notify: reload ssh



