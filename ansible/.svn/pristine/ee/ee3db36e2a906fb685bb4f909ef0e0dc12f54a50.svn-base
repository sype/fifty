[mysqld]

datadir=/home/mysql
socket=/var/run/mysqld/mysqld.sock
user=mysql
# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0
port=3306
tmpdir          = /dev/shm
#lower_case_table_names=1
skip-external-locking
#replicate-do-db = DB_CENTRAL
slave-skip-errors=all
#key_buffer              = 64M
#JFW  moved from 64M to 264M
max_allowed_packet      = 512M
thread_stack            = 192K
thread_cache_size       = 8
# This replaces the startup script and checks MyISAM tables if needed
# the first time they are touched
myisam-recover         = BACKUP
max_connections        = 1000
table_cache            = 256
#
# * Query Cache Configuration
#
# GL: changed very aggressive defaults
query_cache_limit       = 2M
query_cache_size        = 16M

log_error                = /var/log/mysql/error.log

# Here you can see queries with especially long duration
slow_query_log          = 1
slow_query_log_file     = /var/log/mysql/mysql-slow.log
long_query_time         = 20
expire_logs_days        = 10
max_binlog_size         = 100M
tmp_table_size          = 128M
# 20120921 LE 6->12G (GL advice)
innodb_buffer_pool_size = 6G
innodb_log_buffer_size  = 4M
# 20120921 LE : re-set thread_concurrency to 0 (GL advice)
innodb_thread_concurrency = 0


# 20111206 LE : may cause issues in a perfect SQL world, I try that to table lock issues
innodb_locks_unsafe_for_binlog = 1

# 20120921 LE 128->512M (GL advice)
innodb_log_file_size  = 512M
innodb_flush_log_at_trx_commit  = 2
innodb_flush_method     = O_DIRECT
innodb_file_format      = Barracuda
# GL: use interleaved auto-inc lock mode for more scalable inserts. potentially unsafe on replication with statements.
innodb_autoinc_lock_mode = 2

join_buffer_size        = 1M
sort_buffer_size        = 64M


default-storage-engine  = innodb
innodb_file_per_table
innodb_data_home_dir    = /home/mysql/
#innodb_data_file_path   = ibdata1:10G;ibdata2:10G:autoextend
innodb_fast_shutdown    = 1

# GL: generate more log warnings on connection errors
log_warnings            = 2

[mysqldump]
quick
quote-names
max_allowed_packet      = 16M

[mysql]
#no-auto-rehash # faster start of mysql but no tab completition
compress

[isamchk]
key_buffer              = 16M


[mysqld_safe]
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid

#
# * IMPORTANT: Additional settings that can override those from this file!
#   The files must end with '.cnf', otherwise they'll be ignored.
#
!includedir /etc/mysql/conf.d/

