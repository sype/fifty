#!/bin/python

"""
Move bucket from gs://backup.fifty-five.com/yourbucket to
gs://glacier.fifty-five.com/destination

# $Id: copy_bucket.py 52855 2015-09-11 15:54:18Z luc $
"""

from socket import gethostname
from utils55 import exec_command, print_now


def main():
    """"Main loop"""
    to_save = raw_input("Bucket to save: ")
    exec_command(GSUTIL_SERVER_CMD + 'ls ' + BUCKET_TO_SAVE + to_save + '/')
    destination = raw_input("Destination bucket: ")
    print "%s Saving %s in %s" % \
          (print_now(), BUCKET_TO_SAVE + to_save + '/', DESTINATION_BUCKET + destination + '/')
    exec_command(GSUTIL_SERVER_CMD + '-m mv -p -r %s %s' %
                 (BUCKET_TO_SAVE + to_save + '/', DESTINATION_BUCKET + destination + '/'))


if __name__ == '__main__':
    SERVER = gethostname()
    BOTO_CONFIG_DIR = '$HOME/'
    BOTO_CONFIG_NAME = '.boto_dash55'
    BUCKET_TO_SAVE = 'gs://glacier.fifty-five.com/'
    DESTINATION_BUCKET = 'gs://backup.fifty-five.com/'
    GSUTIL_SERVER_CMD = 'BOTO_CONFIG=%s%s gsutil ' % \
                        (BOTO_CONFIG_DIR, BOTO_CONFIG_NAME)

    print '### START: ' + print_now() + ' ###'
    main()
    print '### END: ' + print_now() + ' ###'
