#!/bin/python

"""
Upload customer data older than "backup_time" on Google
Cloud Storage gs://glacier.fifty-five.com/ bucket and delete it
from local source.

# $Id: backup_customer.py 52224 2015-08-25 13:23:26Z luc $
"""

import os
from time import time
from socket import gethostname
from utils55 import exec_command, print_now

def main():
    """"Main loop"""
    if SERVER == 'ks25974.kimsufi.com':
        paths = ['/home/var/www']
    elif SERVER == 'in01.55labs.com':
        paths = ['/home/ftp', '/home/sftp']

    for path in paths:
        path_split = path.split('/')
        path_len = len(path_split)
        for root, dirs, files in os.walk(path):
            for name in files:
                filename_path = os.path.join(root, name)
                fp_split = filename_path.split("/")
                customer = fp_split[path_len]
                path = customer + '/' + SERVER + filename_path + '"'
                backup_time = check_time(customer)
                if os.stat(filename_path).st_mtime < NOW - (backup_time * 86400):
                    new_cmd = GSUTIL_SERVER_CMD + '"' + filename_path + '" ' + BUCKET_PATH + path
                    print "%s Uploading... %s backup on Google Cloud Storage" % \
                          (print_now(), filename_path)
                    exec_command(new_cmd)
                    print "%s Backup of %s on %s is done!" % \
                          (print_now(), filename_path, BUCKET_PATH + path)
                    print "%s Deleting... %s from %s" % (print_now(), filename_path, SERVER)
                    exec_command('rm "' + filename_path + '"')
                    print "%s %s is deleted" % (print_now(), filename_path)


def check_time(customer):
    """Return the good backup time for the customer"""
    for key, value in CUSTOM_BACKUP_TIME.items():
        if customer.startswith(key):
            return value
    return DEFAULT_BACKUP_TIME


if __name__ == '__main__':
    NOW = time()
    SERVER = gethostname()
    BOTO_CONFIG_DIR = '$HOME/'
    BOTO_CONFIG_NAME = '.boto_dash55'
    BUCKET_PATH = '"gs://glacier.fifty-five.com/'
    GSUTIL_SERVER_CMD = 'BOTO_CONFIG=%s%s gsutil cp ' % \
                        (BOTO_CONFIG_DIR, BOTO_CONFIG_NAME)
    DEFAULT_BACKUP_TIME = 40
    CUSTOM_BACKUP_TIME = {'disney': 50, 'ferrero': 120, 'yvesrocher': 1, 'activinstinct': 30}

    print '### START: ' + print_now() + ' ###'
    main()
    print '### END: ' + print_now() + ' ###'
