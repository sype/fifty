#!/bin/python

"""
Fifty-five python tools library

# $Id: utils55.py 52855 2015-09-11 15:54:18Z luc $
"""

from time import strftime
from subprocess import Popen, PIPE


def print_now():
    """"Print debug time"""
    return "[" + strftime('%d/%m/%Y') + " " + strftime('%H:%M:%S') + "]"


def exec_command(command):
    """"Launch cli process"""
    pobj = Popen(command, stdout=PIPE, stderr=PIPE, shell=True)
    stdout_data, stderr_data = pobj.communicate()

    if pobj.returncode != 0:
        raise RuntimeError('%r failed, status code %s stdout %r stderr %r' %
                           (command, pobj.returncode, stdout_data, stderr_data))
    return stdout_data
