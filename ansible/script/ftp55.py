#!/bin/python

"""
Fifty-five python ftp library

# $Id: ftp55.py 52855 2015-09-11 15:54:18Z luc $
"""

import ftplib
import datetime
from utils55 import exec_command, print_now


def connection():
    """Connect to the good ftp server"""
    login = ''
    password = ''
    proc = exec_command('bash -i -c alias | grep -o "ftpback-.*net"')
    machine = proc.split()

    with open('/home/runner/.netrc', 'rU') as lines:
        for line in lines:
            line = line.strip().split()
            if line[-1] == machine[0]:
                line = lines.next().strip().split()
                login = line[-1]
                line = lines.next().strip().split()
                password = line[-1]
                break

    try:
        print '%s try to connect on %s' % (print_now(), machine[0])
        ftp = ftplib.FTP(machine[0])
        ftp.login(login, password)
    except ValueError, error:
        print error
    else:
        print '%s connect on %s' % (print_now(), machine[0])
        return ftp


def upload(save_name, name_path, ftp):
    """Upload to ftp"""
    now = datetime.datetime.now()
    day = now.strftime('%w')   # $day = 0..6, 0 is sunday
    one_week = now - datetime.timedelta(days=7)
    two_week = now - datetime.timedelta(days=14)
    name = save_name + "." + day + ".zip"

    if day == "6":
        last_week = one_week.strftime('%Y-%m-%d_') + name
        last_2week = two_week.strftime('%Y-%m-%d_') + name
        try:
            ftp.delete(last_2week)
        except:
            pass
        try:
            ftp.rename(last_week, last_2week)
        except:
            pass
        try:
            ftp.rename(name, last_week)
        except:
            pass
    print "%s Uploading... %s on ftp server" % (print_now(), name)
    ftp.storbinary("STOR " + name, open(name_path + ".zip", "rb"), 1024)
    print "%s Backup of %s is done!" % (print_now(), name)


def chdir(ftp_path, ftp):
    """Change chdir on ftp"""
    dirs = [d for d in ftp_path.split('/') if d != '']
    for folder in dirs:
        check_dir(folder, ftp)
        ftp.cwd(folder)


def check_dir(folder, ftp):
    """Create a dir id doesn't exist on ftp"""
    list_dir = []
    ftp.retrlines('LIST', list_dir.append)
    found = False

    for line in list_dir:
        if line.split()[-1] == folder and line.lower().startswith('d'):
            found = True

    if not found:
        ftp.mkd(folder)
