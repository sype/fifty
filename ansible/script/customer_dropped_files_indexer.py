#!/bin/python

"""
Send customer data was last modified 24 hours ago
from sftp and in01 to elastic search

# $Id: customer_dropped_files_indexer.py 52855 2015-09-11 15:54:18Z luc $
"""

import os
import time
import json
from requests import post
from datetime import datetime
from socket import gethostname
from utils55 import exec_command, print_now


def main():
    """"Main loop"""
    if SERVER == 'ks25974.kimsufi.com':
        paths = ['/home/var/www']
    elif SERVER == 'in01.55labs.com':
        paths = ['/home/ftp', '/home/sftp']

    elastic_post(paths)


def elastic_post(paths):
    """Find last deposed files, generate json file and send it to elastic search"""
    for path in paths:
        path_split = path.split('/')
        path_len = len(path_split)
        command = 'sudo find %s -type f -mtime 1' % (path)
        lines = exec_command(command).splitlines()
        for line in lines:
            file_infos = get_file_infos(line)
            json_request = build_json(line, file_infos, path_len)
            result = json.dumps(json_request)
            stdout = post(URI + json_request.get('_id'), data=result)
            print "%s %s" % (print_now(), stdout.text)


def get_file_infos(line):
    """Build file infos dictionnary"""
    file_infos = {}
    created = time.ctime(os.path.getctime(line))
    created = datetime.strptime(created, '%a %b %d %H:%M:%S %Y')
    file_infos["created"] = datetime.strftime(created, '%Y-%m-%d %H:%M:%S:000')
    file_infos["filename"] = os.path.splitext(os.path.basename(line))[0]
    return file_infos


def build_json(line, file_infos, path_len):
    """build json request for elastic search"""
    line_split = line.split('/')
    json_request = {}
    json_request["customerId"] = line_split[path_len]
    line_split.pop()
    json_request["_id"] = SERVER + ''.join(line_split) + file_infos.get('filename') + TODAY
    json_request["fileName"] = file_infos.get('filename') + TODAY
    json_request["filePath"] = line
    json_request["fileSize"] = os.path.getsize(line)
    json_request["fileDate"] = TODAY
    json_request["timestamp"] = file_infos.get('created')
    return json_request


if __name__ == '__main__':
    TODAY = time.strftime('%Y%m%d')
    URI = 'http://esdev01.55labs.com:9200/tt_delivered_files/delivered_files/'
    SERVER = gethostname()
    print '### START: ' + print_now() + ' ###'
    main()
    print '### END: ' + print_now() + ' ###'
