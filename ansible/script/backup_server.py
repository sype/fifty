#!/bin/python

"""
Backup server configs and customers data on ftp and google cloud storage
Note: For customer data it only save files that were deposited one day before

# $Id: backup_server.py 52857 2015-09-11 16:35:45Z luc $
"""

import ftp55
import os
import sys
from time import strftime
from shutil import make_archive
from socket import gethostname
from utils55 import exec_command, print_now


def main():
    """"Main loop"""
    if SERVER == 'ks25974.kimsufi.com':
        paths = ['/home/var/www']
    elif SERVER == 'in01.55labs.com':
        paths = ['/home/ftp', '/home/sftp']

    server_data_backup()
    client_data_backup(paths)

    print "%s Uploading... %s backup on Google Cloud Storage" % (print_now(), SERVER)
    exec_command(GSUTIL_SERVER_CMD)
    print "%s Backup of %s on %s is done!" % \
              (print_now(), SERVER, BUCKET_PATH + "/" + TODAY)

    exec_command('rm -rf %s' % BACKUP_TMP)


def server_data_backup():
    """Save server's configs data on ftp"""
    for data in SERVER_DATA:
        zip_name = data.split('/')
        zip_name = zip_name[-1]
        cmd_zip = 'sudo zip -rq %s.zip %s'% ('configs/' + zip_name, data)
        os.makedirs('configs/' + zip_name)
        print '%s Compressing... %s' % (print_now(), data)
        exec_command(cmd_zip)
        print '%s Compression succeed... %s' % (print_now(), data)
        ftp55.chdir('configs', FTP)
        ftp55.upload(zip_name, 'configs/' + zip_name, FTP)
        FTP.cwd('/')
        exec_command('rm -rf %s' % (BACKUP_TMP + '/configs/' + zip_name))


def client_data_backup(paths):
    """Save server's customers data on ftp"""
    for path in paths:
        path_split = path.split('/')
        path_len = len(path_split)
        service = ''.join((path_split[-1:]))
        command = 'sudo find %s -type f -mtime 1' % (path)
        lines = exec_command(command).splitlines()
        for line in lines:
            line_split = line.split('/')
            customer = line_split[path_len]
            path = 'customers/' + service + '/' + customer + line
            if not os.path.exists(path):
                os.makedirs(path)
            cmd_copy = 'sudo cp "%s" "%s"' % (line, path)
            exec_command(cmd_copy)

    service_folder = os.listdir('customers/')
    for service in service_folder:
        customer_folder = os.listdir('customers/' + service + '/')
        for customer in customer_folder:
            path = 'customers/' + service + '/' + customer
            print '%s Compressing... %s' % (print_now(), path)
            make_archive(path, 'zip', path)
            print '%s Compression succeed... %s' % (print_now(), path)
            ftp55.chdir(path, FTP)
            ftp55.upload(customer, 'customers/' + service + '/' + customer, FTP)
            FTP.cwd('/')
            exec_command('rm -rf %s' % (path))


if __name__ == '__main__':
    TODAY = strftime('%Y/%m/%d')
    BACKUP_TMP = '/home/runner/backup/backuptmptest'
    SERVER = gethostname()
    BOTO_CONFIG_DIR = '$HOME/'
    BOTO_CONFIG_NAME = '.boto_dash55'
    BUCKET_PATH = 'gs://backup.fifty-five.com/' + SERVER + '/' + TODAY
    GSUTIL_SERVER_CMD = 'BOTO_CONFIG=%s%s gsutil -m cp -r ./ %s' % \
                 (BOTO_CONFIG_DIR, BOTO_CONFIG_NAME, BUCKET_PATH)
    SERVER_DATA = ['/etc/apache2', '/etc/ssh', '/etc/ssl']
    FTP = ftp55.connection()

    if FTP is None:
        sys.exit('No connection to a ftp available in /home/runner/.netrc')

    print '### START: ' + print_now() + ' ###'
    os.makedirs(BACKUP_TMP)
    os.chdir(BACKUP_TMP)
    main()
    print '### END: ' + print_now() + ' ###'

    FTP.quit()
