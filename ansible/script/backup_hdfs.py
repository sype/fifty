#!/bin/python

"""
Backup customer hdfs files into google cloud storage with a selected period

Usage: python backup_hdfs.py -c "DISNEY" --start "2015-07-01"
--stop "2015-08-01" --delete

Options:
    -c --customer: customer name ex (--customer "DISNEY FBI 3SUISSES")
    --start: date to backup ex (--start 2014-08-01")
    --stop: end of backup date ex(--stop "2015-08-01")
    --delete: delete files from hdfs after backup

$Id: backup_hdfs.py 52855 2015-09-11 15:54:18Z luc $
"""


import os
import re
import sys
from datetime import datetime
from argparse import ArgumentParser
from socket import gethostname
from utils55 import exec_command, print_now


def main():
    """"Main loop"""
    customer_list = [x.upper() for x in ARGS.customer.split()]
    command = 'hdfs dfs -ls -R /DATA/IN/'
    regex = '/DATA/IN/'
    paths = []

    try:
        start_date_to_find = datetime.strptime(ARGS.start, '%Y-%m-%d')
        stop_date_to_find = datetime.strptime(ARGS.stop, '%Y-%m-%d')
        if start_date_to_find > NOW:
            raise ValueError("Error: Start date (--start) cannot be greater than now")
        if start_date_to_find > stop_date_to_find:
            raise ValueError("Error: Start date (--start) cannot be greater than end date (--stop)")
    except ValueError as inst:
        sys.exit(inst)

    for customer in customer_list:
        command += customer
        regex += customer + '.+/' + r'\d{4}/\d{2}/\d{2}'
        paths = get_path_list(start_date_to_find, stop_date_to_find, customer, command, regex)

    backup(paths, regex)


def get_path_list(start, stop, customer, command, regex):
    """Get list files from hdfs thats match with selected date"""
    paths = []
    stdout = exec_command(command)
    stdout = re.findall(regex, stdout)
    stdout = list(set(stdout))

    if len(stdout) == 0:
        sys.exit("Error: Customer: %s not found" % (customer))
    for lines in stdout:
        path = re.findall(r'\d{4}/\d{2}/\d{2}', lines)
        try:
            lines += " " + path[0]
        except ValueError:
            lines += ' 1970/01/01'
        lines_split = lines.split()
        date_tmp = datetime.strptime(lines_split[1], '%Y/%m/%d')
        if start <= date_tmp <= stop:
            paths.append(lines_split[0])
    print "%s List of files created" % (print_now())
    return paths


def backup(path_list, regex):
    """Copy hdfs files into local directory and upload them in google cloud storage"""
    regex_split = regex.split('+/')
    regex_compile = re.compile(regex_split[1])

    for path in path_list:
        search_date = re.search(regex_compile, path)
        date = search_date.group()
        backup_path = BACKUP_TMP + path

        os.makedirs(backup_path)
        print "%s Copying... hdfs: %s to local: %s" % (print_now(), path, backup_path)
        exec_command('hdfs dfs -copyToLocal ' + path + "/* " + backup_path)
        print "%s Copy is done" % (print_now())
        print "%s Uploading... %s on Google Cloud Storage" % (print_now(), backup_path)
        exec_command(GSUTIL_CMD + date + "/")
        print "%s Backup of %s on %s is done!" % \
              (print_now(), backup_path, BUCKET_PATH + date + "/")
        if ARGS.delete:
            print "%s Deleting %s from hdfs" % (print_now(), path)
            exec_command('sudo -u hdfs hdfs dfs -rm -r -skipTrash ' + path)
        exec_command('rm -rf ' + BACKUP_TMP)


if __name__ == '__main__':
    PARSER = ArgumentParser()
    PARSER.add_argument("-c", "--customer", required=True,
                        help='Customer you want to save. ex: "DISNEY FBI"')
    PARSER.add_argument("--start", required=True,
                        help='Start date from your the backup. ex: "2014-07-09"')
    PARSER.add_argument("--stop", required=True,
                        help='Stop date from your the backup. ex: "2014-07-10"')
    PARSER.add_argument("--delete", help='Delete files from hdfs', action="store_true")
    ARGS = PARSER.parse_args()
    NOW = datetime.now()
    BACKUP_TMP = "/home/runner/muliBase/backupHdfs"
    SERVER = gethostname()
    BOTO_CONFIG_DIR = "$HOME/"
    BOTO_CONFIG_NAME = ".boto_dash55"
    BUCKET_PATH = "gs://backup.fifty-five.com/" + SERVER + "/"
    GSUTIL_CMD = "BOTO_CONFIG=%s%s gsutil cp -r %s/* %s" % \
                 (BOTO_CONFIG_DIR, BOTO_CONFIG_NAME, BACKUP_TMP, BUCKET_PATH)

    print "### START: " + print_now() + " ###"
    main()
    print "### END: " + print_now() + " ###"
