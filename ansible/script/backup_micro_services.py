#!/bin/python

"""
Backup of micro services files from  MongoDB, apache,
upstart and "/etc/", "/var/opt/", "/var/www/" on FTP and Google Cloud Storage

# $Id: backup_micro_services.py 52854 2015-09-11 15:24:09Z luc $
"""


import os
import ftp55
import sys
from shutil import make_archive
from socket import gethostname
from time import strftime
from utils55 import exec_command, print_now


def main():
    """Main Loop"""
    # save mongoDB
    for service in SERVICES:
        service_tmp = service
        service_tmp = "ca" if service_tmp == "customer_activation" else service_tmp
        mongo_tmp = BACKUP_TMP + service_tmp + ".mongo"
        mongo_cmd = 'sudo mongodump --quiet --db %s -o %s' % (service, BACKUP_TMP)
        print "%s Export... %s" % (print_now(), service)
        exec_command(mongo_cmd)
        print "%s Compressing... %s" % (print_now(), service)
        make_archive(mongo_tmp, 'zip', BACKUP_TMP + service)
        ftp55.upload(service_tmp + ".mongo", mongo_tmp, FTP)
        exec_command('sudo rm -rf %s' % BACKUP_TMP + service)

    SERVICES[0] = "ca" ## replace customer_activation by "ca"

    # save all other files
    for service in SERVICES:
        path = BACKUP_TMP + service + ".files"
        cmd_zip = 'zip -rq %s.zip ' % path
        os.mkdir(path)
        cmd_zip += glob("api-" + service, APACHE_PATH)
        cmd_zip += glob(service + ".conf", UPSTART_PATH)
        cmd_zip += residual(service)
        print "%s Compressing... %s" % (print_now(), service)
        exec_command(cmd_zip)
        ftp55.upload(service + ".files", path, FTP)
        exec_command('sudo rm -rf %s' % BACKUP_TMP + service + ".files")

    # upload on google cloud storage
    print "%s Uploading... all micro services on Google Cloud Storage" % (print_now())
    exec_command(GSUTIL_SERVER_CMD)
    print "%s Backup of all micro services on %s is done!" % \
          (print_now(), 'gs://backup.fifty-five.com/' + SERVER + TODAY)

    FTP.quit()
    exec_command('rm -rf %s' % BACKUP_TMP)


def glob(name, path):
    """Save all files match with a given name in a given path"""
    files = []
    files_path = ''

    for (root, dirs, file_names) in os.walk(path):
        files.extend(file_names)
        for word in files:
            if word.startswith(name):
                print "%s Saving... %s" % (print_now(), path + word)
                files_path += path + word + ' '
        break
    return files_path


def residual(name):
    """Save etc/[microservice] var/opt/[microservice] var/www/[microservice]"""
    paths = ["/etc/", "/var/opt/", "/var/www/", "/usr/share/"]
    files_path = ''

    for path in paths:
        print "%s Saving... %s" % (print_now(), path + name)
        files_path += path + name + ' '
    return files_path


if __name__ == '__main__':
    SERVICES = ["customer_activation", "iap", "mobile", "storage"]
    BACKUP_TMP = "/home/runner/backup/backupMicroServices/"
    APACHE_PATH = "/etc/apache2/sites-available/"
    UPSTART_PATH = "/etc/init/"
    TODAY = strftime('%Y/%m/%d')
    SERVER = gethostname() + "/"
    GSUTIL_SERVER_CMD = 'sudo gsutil -m -q cp -r %s* gs://backup.fifty-five.com/%s' %\
                        (BACKUP_TMP, SERVER + TODAY)

    FTP = ftp55.connection()
    if FTP is None:
        sys.exit("No connection to a ftp available in /home/runner/.netrc")

    print "### START: " + print_now() + " ###"
    os.mkdir(BACKUP_TMP)
    main()
    print "### END: " + print_now() + " ###"
