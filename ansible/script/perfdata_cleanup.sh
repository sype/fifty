#!/bin/bash

find /var/spool/icinga2/perfdata/service-perfdata.* -ctime +2 -exec rm {} \;
find /var/spool/icinga2/perfdata/host-perfdata.* -ctime +2 -exec rm {} \;
