#!/bin/python

"""
Raw_data to hdfs script

Usage: python backup_hdfs.py --customer 3SUISSES --date 2015-07-01
--account UA-27184057-2

Options:
    --date: date to export ex (--date 2014-08-01)
    --customer: Customer you want to export ex (--customer 3SUISSES)
    --account: Universal Analytics account ex (--account UA-27184057-2)

$Id: raw_data_to_hdfs.py 52937 2015-09-15 16:57:18Z luc $
"""

import os
import re
import gzip
import datetime
import urllib
import json
import sys
from argparse import ArgumentParser
from utils55 import exec_command, print_now


def main():
    """"Main loop"""
    paths = []
    # ls_cmd = "BOTO_CONFIG=%s gsutil ls %s" % \
                 #(os.path.join(BOTO_CONFIG_DIR, BOTO_CONFIG_NAME), BUCKET_PATH)
    ls_cmd = "gsutil ls %s" % BUCKET_PATH
    stdout = exec_command(ls_cmd).splitlines()
    for line in stdout:
        if re.search(r'collect.*rawdata-app', line):
            paths.append(line)
            print "%s Path to export: %s" % (print_now(), line)

    for path in paths:
        xfiles = path.split('/')
        filename = CUSTOMER + '.' + xfiles[-1] + '.gz'
        #cat_cmd = "BOTO_CONFIG=%s gsutil cat %s" % \
             #(os.path.join(BOTO_CONFIG_DIR, BOTO_CONFIG_NAME), path)
        cat_cmd = "gsutil cat %s" % path
        print "%s Creating %s" % (print_now(), filename)
        gz_file = gzip.open(BACKUP_TP9 + filename, 'ab')
        stdout = exec_command(cat_cmd).splitlines()
        for line in stdout:
            if re.search(ARGS.account, line):
                request = build_request(line)
                gz_file.write('GET /55.gif ?' + request + '&end=end\n')
        gz_file.close()

    print "%s Copy all files to haprod01" % print_now()
    exec_command("scp -r %s* runner@haprod01.55labs.com:%s" % (BACKUP_TP9, BACKUP_HA1))
    exec_command("ssh runner@haprod01.55labs.com sudo -u hdfs hdfs dfs -mkdir -p " + EXPORT_PATH)
    print "%s Export all files to hdfs: %s" % (print_now(), EXPORT_PATH)
    exec_command("ssh runner@haprod01.55labs.com sudo -u hdfs hdfs dfs -put -f " + BACKUP_HA1
                 + "* " + EXPORT_PATH)
    print "%s Export done!" % print_now()


def build_request(line):
    """"Build request"""
    split_line = line.split(';')
    date = split_line[0]
    date_split = date.split(" ")
    day = date_split[0]
    time_tmp = date_split[1]
    time_split = time_tmp.split('.')
    time = time_split[0].replace(':', '-')
    request = urllib.unquote(split_line[5])
    request += '&remoteip=' + split_line[6]
    request += '&sdate=' + day
    request += '&shour=' + time
    request += '&v=' + urllib.unquote(split_line[7])
    return request


def check_account():
    """"Check if UA account match with the customer"""
    try:
        response = urllib.urlopen(ID_URI)
    except IOError:
        sys.exit("Could not connect to %s (firewall ?)" % ID_URI)
    data = json.loads(response.read())
    for item in data:
        value = json.loads(item["value"])
        if item["key"] == ARGS.account and value["instanceId"] == CUSTOMER:
            return
    sys.exit("Error: account doesn't match with customer")


if __name__ == '__main__':
    PARSER = ArgumentParser()
    PARSER.add_argument("--date", required=True,
                        help='Date to export. ex: "2014-07-09"')
    PARSER.add_argument("--account", required=True,
                        help='UA account. ex: "UA-27184057-2"')
    PARSER.add_argument("--customer", required=True,
                        help='Customer you want to save. ex: "3SUISSES"')
    ARGS = PARSER.parse_args()
    BACKUP_TP9 = "/home/runner/mtp_rawdata_tp9/"
    BACKUP_HA1 = "/home/runner/mtp_rawdata_ha1/"
    ID_URI = 'https://api-storage.55labs.com:5004/v1/RAWDATA/RAWDATA/configs/customer.properties'
    #BOTO_CONFIG_DIR = "$HOME/"
    #BOTO_CONFIG_NAME = ".boto_dash55"
    DATE = datetime.datetime.strptime(ARGS.date, '%Y-%m-%d')
    CUSTOMER = ARGS.customer.upper()
    EXPORT_DATE = datetime.date.strftime(DATE, '%Y/%m/%d/')
    BUCKET_PATH = "gs://55_rawdata/in/" + ARGS.date + "/"
    EXPORT_PATH = "/DATA/IN/%s/%s/MOVER/WEBLOGS/%s" % (CUSTOMER, CUSTOMER, EXPORT_DATE)

    print "### START: " + print_now() + " ###"
    try:
        check_account()
        os.makedirs(BACKUP_TP9)
        exec_command("ssh runner@haprod01.55labs.com mkdir -p %s" % BACKUP_HA1)
        main()
    finally:
        exec_command("rm -rf %s" % BACKUP_TP9)
        exec_command("ssh runner@haprod01.55labs.com rm -rf %s" % BACKUP_HA1)
    print "### END: " + print_now() + " ###"
